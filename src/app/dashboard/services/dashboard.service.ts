import { Injectable } from '@angular/core';
import { CHttp } from '../../core/services/chttp.service';
import { config } from '../../app.config';

@Injectable()
export class DashboardService {

  constructor(private http: CHttp) { }

  public getStats(data?: any) {
    return this.http.get(config.api_url + 'dashboard/global-stats', {
      params: data
    });
  }

  public getStatusStats(data?: any) {
    return this.http.get(config.api_url + 'dashboard/status-stats', {
      params: data
    });
  }

  public getConversionStats(data?: any) {
    return this.http.get(config.api_url + 'dashboard/conversion-stats', {
      params: data
    });
  }

  public getRetentionStats(data?: any) {
    return this.http.get(config.api_url + 'dashboard/retention-stats', {
      params: data
    });
  }

  public getTopOffers(data?: any) {
    return this.http.get(config.api_url + 'dashboard/top-offers', {
      params: data
    });
  }

  public getTopUsers(data?: any) {
    return this.http.get(config.api_url + 'dashboard/top-users', {
      params: data
    });
  }

}
