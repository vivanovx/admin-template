import { Component, OnInit } from '@angular/core';
import { LocationService } from '../../locations/services/location.service';
import { NavigationStart, Router, Params, ActivatedRoute } from '@angular/router';
import { RouteHelperService } from '../../core/services/route-helper.service';
import { DashboardService } from '../services/dashboard.service';
import * as moment from 'moment';
import { CTranslateService } from '../../core/services/translate.service';
import { BaseComponent } from '../../core/common/base-component';

@Component({
  selector: 'ch-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent extends BaseComponent {

  public globalStats: any = {
    total: 321,
    registered: 302,
    active: 270,
    approved: 190,
    blocked: 43
  };

  constructor(private translate: CTranslateService) {
    super();

    (async () => {
      this.texts.dashboard = await translate.getTranslatedText('dashboard:sidebar');
    })();
  }

}
