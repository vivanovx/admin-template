import { Component, OnInit } from '@angular/core';
import { Params, Router, ActivatedRoute } from '@angular/router';
import { PagesService } from '../services/pages.service';
import { FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';
import { RouteHelperService } from '../../core/services/route-helper.service';
import { BaseEditComponent } from '../../core/common/base-component';
import { AlertService } from '../../core/services/alert.service';
import { CTranslateService } from '../../core/services/translate.service';

@Component({
  selector: 'ch-page-edit',
  templateUrl: './page-edit.component.html',
  styleUrls: ['./page-edit.component.css']
})
export class PageEditComponent extends BaseEditComponent implements OnInit {

  private pageId: string;
  public pageData: any = {};
  public pageName: string;
  public initialContent: string;

  public errors = {};

  constructor(
    private alertService: AlertService,
    private pagesService: PagesService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private routeHelperService: RouteHelperService,
    private translate: CTranslateService
  ) { super(); }

  ngOnInit() {
    this.activatedRoute.queryParamMap
      .map((params: Params) => params.params)
      .subscribe((params) => {
        this.editMode = this.routeHelperService.isCurrentPageEdit();
        this.pageId = params.id;

        if (this.editMode) {
          this.fetchData(this.pageId);
        }
      });

    this.setTexts();
  }

  private async setTexts() {
    this.texts.save = await this.translate.getTranslatedText('save:edit');
    this.texts.addNew = await this.translate.getTranslatedText('add-new:edit');
    this.texts.invalidData = await this.translate.getTranslatedText('invalid-data:edit');
    this.texts.staticPages = await this.translate.getTranslatedText('static-pages:sidebar');
    this.texts.deleteConfirmation = await this.translate.getTranslatedText('delete-confirmation:edit');
    this.texts.deleteUsedError = await this.translate.getTranslatedText('delete-used-error:edit');
    this.texts.deleteError = await this.translate.getTranslatedText('delete-error:edit');
  }

  private fetchData(id: string) {
    this.loading = true;
    this.pagesService.getPage(id)
      .subscribe(data => {
        this.pageData = data.json();
        this.pageName = this.pageData['name'];
        this.initialContent = this.pageData.content;
      }, () => { }, () => { this.loading = false; this.loaded = true; });
  }

  public contentChange(value) {
    this.pageData.content = value;
  }

  public edit(form) {
    this.setFormLoading(true);
    if (!form.valid) {
      for (const i of Object.keys(form.controls)) {
        form.controls[i].markAsTouched();
      }

      this.setFormLoading(false);
      this.alertService.error(this.texts.invalidData);
      return;
    }

    this.loading = true;
    const methodName = this.editMode ? 'updatePage' : 'addPage';
    this.pagesService[methodName](this.pageData)
      .subscribe(
      result => {
        this.cancel();
      },
      error => {
        for (const item of error.errors instanceof Array ? error.errors : []) {
          if (!form.controls[item.field]) {
            continue;
          }
          form.controls[item.field].setErrors({ [item.field]: item.message });
          this.errors[item.field] = item.message;
        }
      },
      () => { this.loading = false; this.setFormLoading(false); }
      );
  }

  public cancel() {
    this.router.navigateByUrl('/admin/pages');
  }

  public delete() {
    if (!confirm(this.texts.deleteConfirmation)) {
      return;
    }

    this.pagesService.deletePage(this.pageId)
      .subscribe(
      result => { this.cancel(); },
      error => {
        if (409 === error.status) {
          this.alertService.error(this.texts.deleteUsedError);
        } else {
          this.alertService.error(this.texts.deleteError);
        }
      }
      );
  }

}
