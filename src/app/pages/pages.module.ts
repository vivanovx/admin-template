import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { CoreModule } from '../core/core.module';
import { PagesListingComponent } from './pages-listing/pages-listing.component';
import { PagesService } from './services/pages.service';
import { PageEditComponent } from './page-edit/page-edit.component';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    CoreModule,
    FormsModule,
    TranslateModule.forRoot()
  ],
  exports: [
    PagesListingComponent,
    PageEditComponent
  ],
  providers: [
    PagesService
  ],
  declarations: [PagesListingComponent, PageEditComponent]
})
export class PagesModule { }
