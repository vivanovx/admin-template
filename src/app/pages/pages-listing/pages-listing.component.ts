import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { PagesService } from '../services/pages.service';
import { SortableComponent } from '../../core/sortable/sortable.component';
import { RouteHelperService } from '../../core/services/route-helper.service';

@Component({
  selector: 'ch-pages-listing',
  templateUrl: './pages-listing.component.html',
  styleUrls: ['./pages-listing.component.css']
})
export class PagesListingComponent implements OnInit {

  public items = [];
  public totalItems;
  public pageParams: any = {};

  public loading: boolean;

  constructor(
    private pagesService: PagesService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private routeHelperService: RouteHelperService
  ) { }

  ngOnInit() {
    this.activatedRoute.queryParamMap
      .map((params: Params) => params.params)
      .subscribe((params) => {
        this.pageParams = this.routeHelperService.getValidatedParams(params);

        this.loadItems(this.pageParams);
      });
  }

  public edit(id: string) {
    this.router.navigate(['/admin/pages/edit'], {
      queryParams: {
        id: id
      }
    });
  }

  public addNew() {
    this.router.navigate(['/admin/pages/add']);
  }

  public search(event) {
    this.queryParams({
      query: event,
    });
  }

  public onSort(data) {
    this.queryParams({
      sortBy: data.column,
      sortOrder: data.order,
    });
  }

  public onPageChanged(pageNumber) {
    this.queryParams({
      pageNumber: pageNumber,
    });
  }

  private queryParams(params) {
    const result = this.routeHelperService.queryParams(params, this.pageParams);
    this.router.navigate([], { queryParams: result });
  }

  private loadItems(data) {
    this.loading = true;
    this.pagesService.getPages(data)
      .subscribe(result => {
        const jsonData = result.json();
        this.items = jsonData;
        this.totalItems = jsonData.totalItems;
        this.loading = false;
      });
  }
}
