import { Injectable } from '@angular/core';
import { CHttp } from '../../core/services/chttp.service';
import { config } from '../../app.config';

@Injectable()
export class PagesService {

  constructor(private http: CHttp) { }

  public getPages(data?: any) {
    return this.http.get(config.api_url + 'pages', { params: data });
  }

  public getPage(id: string) {
    return this.http.get(config.api_url + 'pages/' + id);
  }

  public updatePage(data: {}) {
    return this.http.put(config.api_url + 'pages', data);
  }

  public addPage(data: {}) {
    return this.http.post(config.api_url + 'pages', data);
  }

  public deletePage(id: string) {
    return this.http.delete(config.api_url + 'pages/' + id);
  }

}
