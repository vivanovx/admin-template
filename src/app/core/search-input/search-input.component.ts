import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { CTranslateService } from '../services/translate.service';

@Component({
  selector: 'ch-search-input',
  templateUrl: './search-input.component.html',
  styleUrls: ['./search-input.component.css']
})
export class SearchInputComponent implements OnInit {
  @Input() public value: string;
  @Input() public placeholder: string;
  @Output() public search: EventEmitter<any> = new EventEmitter();
  public searchText: string;

  constructor(private translate: CTranslateService) { }

  ngOnInit() {
    (async () => {
      this.searchText = await this.translate.getTranslatedText('search:core');
    })();
  }

  public submit(value) {
    this.search.emit(value);
  }
}
