import {
  Component,
  Input,
  Output,
  ViewChild,
  OnInit,
  OnChanges,
  EventEmitter,
  ElementRef,
  Renderer2
} from '@angular/core';
import { RequestOptions, Headers, Http } from '@angular/http';
import { CHttp } from '../../core/services/chttp.service';
import { ResourceService } from '../services/resource.service';
import { AuthDataStorageService } from '../../auth/services/auth-data-storage.service';
import jquery from 'jquery';
import swal from 'sweetalert2';

@Component({
  selector: 'ch-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.css']
})
export class FileUploadComponent implements OnInit, OnChanges {
  @Input() public resourceId: string;
  @Input() public maxWidth: number;
  @Input() public maxHeight: number;

  public cvData: any;

  @Output() public changed: EventEmitter<string> = new EventEmitter();
  @ViewChild('inp') private fileInp: ElementRef;
  private endPoint: string;
  private token: string;

  public loading: boolean;

  constructor(
    private http: CHttp,
    private rd: Renderer2,
    private resouceService: ResourceService,
    private authDataStorageService: AuthDataStorageService
  ) {
    this.token = this.authDataStorageService.getSessionTokenFromStorage();
  }

  ngOnInit() { }

  ngOnChanges(changes) {
    if (changes && changes.resourceId && changes.resourceId.currentValue) {
      this.cvData = { resourceId: changes.resourceId.currentValue };
    }
  }

  fileChange(event) {
    const _URL = window.URL || window['webkitURL'];
    const file = this.fileInp.nativeElement.files[0];
    if (file) {
      this.doUpload(event);
    }
  }

  private doUpload(files) {
    this.loading = true;
    this.resouceService.uploadFile(files)
      .subscribe(
      data => {
        this.resourceId = data.resourceId;
        this.cvData = { resourceId: this.resourceId };
        this.changed.emit(data.resourceId);

        this.loading = false;
      },
      error => console.log(error)
      );
  }

  public triggerUpload() {
    this.fileInp.nativeElement.click();
  }

}
