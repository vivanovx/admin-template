import { Component, Input, Output, EventEmitter, OnInit, OnChanges } from '@angular/core';

@Component({
  selector: 'ch-location-picker',
  templateUrl: './location-picker.component.html',
  styleUrls: ['./location-picker.component.css']
})
export class LocationPickerComponent implements OnInit, OnChanges {

  @Input() public locations: any[];
  @Input() public selected: any[] = [];

  @Output() public itemCheck: EventEmitter<any> = new EventEmitter();
  @Output() public itemUncheck: EventEmitter<any> = new EventEmitter();
  @Output() public topLevelSelected: EventEmitter<string[]> = new EventEmitter();

  public items: any;
  private selectedTmp: any[];

  constructor() { }

  ngOnInit() {
    this.items = this.getNodes() || [];
    this.setSelected(this.selected);
  }

  ngOnChanges(changes) {
    if (changes.locations && changes.locations.currentValue instanceof Array) {
      const items = changes.locations.currentValue;
      this.items = items;
      this.items = this.getNodes();

      if (this.selectedTmp) {
        this.setSelected(this.selectedTmp);
        this.selectedTmp = [];
      }
    }
    if (changes.selected && changes.selected.currentValue instanceof Array) {
      const selected = changes.selected.currentValue;
      if (this.items) {
        this.setSelected(selected);
      } else {
        this.selectedTmp = selected;
      }
    }
  }

  private setSelected(ids: string[]) {
    for (const id of ids) {
      this.selectItem(id, true, false);
    }

    this.topLevelSelected.emit(this.getTopLevelSelected());
  }

  private getNodes() {
    const nodes = [];
    for (const item of this.getCountries()) {
      const node: any = { id: item.id, name: item.nameBg };
      const cities = this.getSublocations(item.id);
      if (cities.length) {
        node.children = [];
      }
      for (const city of cities) {
        const cityNode: any = { id: city.id, name: city.nameBg };
        const neighbourhoods = this.getSublocations(city.id);
        if (neighbourhoods.length) {
          cityNode.children = [];
        }
        for (const neighbourhood of neighbourhoods) {
          const neighbourhoodNode: any = { id: neighbourhood.id, name: neighbourhood.nameBg };
          cityNode.children.push(neighbourhoodNode);
        }

        node.children.push(cityNode);
      }

      nodes.push(node);
    }

    return nodes;
  }

  public getCountries() {
    const result = [];
    for (const item of this.locations || []) {
      if (item.isCountry) {
        result.push(item);
      }
    }

    return result;
  }

  public getSublocations(id: string) {
    const result = [];
    for (const item of this.locations || []) {
      if (item.parentLocationId === id) {
        result.push(item);
      }
    }

    return result;
  }

  public checkLocation(item, checked) {
    item.checked = checked;
  }

  /**
   * Item checkbox select handler
   * @param id
   * @param e
   */
  public ch(id: string, e) {
    e.preventDefault();
    this.selectItem(id, e.target.checked, true);
    const topLevelSelected = this.getTopLevelSelected();
    this.topLevelSelected.emit(topLevelSelected);

    return false;
  }

  private disableSubchildren(id: string, b: boolean) {
    const item = this.findItem(id);

    this.disableCheck(item.children || [], b);
  }

  private disableCheck(items: any[], check: boolean) {
    for (const item of items) {
      item.disabled = check;
      item._checked = check;

      const hasChildren = item.children instanceof Array && 0 < item.children.length;
      if (hasChildren) {
        if (!check && item.checked) {
          return;
        }

        this.disableCheck(item.children, check);
      }
    }
  }

  private findItem(id: string) {
    const result = this.getItem(this.items, id);

    return result;
  }

  private getItem(items: any[], id: string) {
    for (const item of items || []) {
      if (item.id === id) { return item; }
      const found = this.getItem(item.children, id);
      if (found) { return found; }
    }

    return null;
  }

  private selectItem(id: string, ch: boolean, emit: boolean = true) {
    this.selectItemR({ items: this.items, id, ch, emit });
  }

  private selectItemR(data: any) {
    const items = data.items || [];
    for (const item of items) {
      const tData = {
        item,
        id: data.id,
        ch: data.ch,
        emit: data.emit
      };
      if (this.selectItem1(tData)) {
        return;
      }

      data.items = item.children;
      this.selectItemR(data);
    }
  }

  private selectItem1(data: any) {
    if (data.item.id === data.id) {
      data.item.checked = data.ch;
      this.disableSubchildren(data.id, data.ch);
      if (data.emit) {
        if (data.ch) {
          this.itemCheck.emit(data.id);
        } else {
          this.itemUncheck.emit(data.id);
        }
      }

      return true;
    }

    return false;
  }

  private getTopLevelSelected() {
    const topLevelSelected = this.getSelected(this.items);

    return topLevelSelected;
  }

  private getSelected(items: any[]) {
    let result: string[] = [];
    for (const item of items || []) {
      if (item.checked) {
        result.push(item.id);
      } else {
        result = result.concat(this.getSelected(item.children));
      }
    }

    return result;
  }

  private setItemChecked(item: any, checked: boolean) {
    item.checked = Boolean(checked);
  }

  public check1(id: string, e) {
    this.checkItem(id, e);

    return false;
  }

  private checkItem(id: string, ch: boolean) {
    for (const a of this.items) {
      if (this.chItem(a, id, ch)) { return; }

      for (const b of a.children || []) {
        if (this.chItem(b, id, ch)) { return; }

        for (const c of b.children || []) {
          if (this.chItem(c, id, ch)) { return; }
        }
      }
    }
  }

  private chItem(o: any, id: string, ch: boolean) {
    if (o.id === id) {
      o.marked = ch;
      // if (ch) {
      //   this.itemCheck.emit(id);
      // } else {
      //   this.itemUncheck.emit(id);
      // }

      return true;
    }

    return false;
  }

  private search(q: string) {
    for (const a of this.items) {
      this.checkFilter(a, q);
      for (const b of a.children || []) {
        this.checkFilter(b, q);

        for (const c of b.children || []) {
          this.checkFilter(c, q);
        }
      }
    }
  }

  private checkFilter(o: any, s: string) {
    delete o.noChildren;
    if ('' === s.trim()) {
      delete o.filter;
      delete o.hidden;

      return;
    }
    if (this.countains(o.name, s)) {
      o.filter = true;
      o.hidden = false;

      o.noChildren = !this.checkFilterChildren(o, s);

      return true;
    } else {
      o.filter = false;
      o.hidden = !this.checkFilterNested(o, s);
    }

    return false;
  }

  private checkFilterNested(o: any, s: string) {
    if (this.countains(o.name, s)) {
      return true;
    }
    const filterChildren = this.checkFilterChildren(o, s);
    o.noChildren = !filterChildren;

    return filterChildren;
  }

  private checkFilterChildren(o: any, s: string) {
    for (const item of o.children || []) {
      if (this.checkFilterNested(item, s)) {
        return true;
      }
    }

    return false;
  }

  private countains(a: string, t: string) {
    if (a.indexOf(t) > -1) {
      return true;
    }

    return false;
  }

  public filterLocations(event) {
    this.search(event.target.value);
    event.preventDefault();

    return false;
  }

}
