import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HeaderComponent } from './header/header.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { BrowserModule } from '@angular/platform-browser';
import { SearchInputComponent } from './search-input/search-input.component';
import { PaginationComponent } from './pagination/pagination.component';
import { LocationFilterInputComponent } from './location-filter-input/location-filter-input.component';
import { AdminComponent } from './admin/admin.component';
import { customHttpProvider } from './services/chttp.service';
import { SortableComponent } from './sortable/sortable.component';
import { LoaderComponent } from './loader/loader.component';
import { RouteHelperService } from './services/route-helper.service';
import { NotFoundComponent } from './not-found/not-found.component';
import { LiveSearchComponent } from './live-search/live-search.component';
import { DaterangepickerComponent } from './daterangepicker/daterangepicker.component';
import { Daterangepicker } from 'ng2-daterangepicker';
import { CheckboxGroupComponent } from './checkbox-group/checkbox-group.component';
import { DatetimePickerComponent } from './datetime-picker/datetime-picker.component';
import { DatetimePickerDirective } from './datetime-picker/datetime-picker.directive';
import { DatepickerComponent } from './datepicker/datepicker.component';
import { DatepickerDirective } from './datepicker/datepicker.directive';
import { DateFormatPipe } from './pipes/date-format.pipe';
import { BreadcrumbComponent } from './breadcrumb/breadcrumb.component';
import { WysiwygComponent } from './wysiwyg/wysiwyg.component';
import { ImageUploadComponent } from './image-upload/image-upload.component';
import { ResourceService } from './services/resource.service';
import { ImageViewComponent } from './image-view/image-view.component';
import { FilePreviewComponent } from './file-preview/file-preview.component';
import { SelectPickerComponent } from './widgets/select-picker/select-picker.component';
import { ImageViewerModule } from '@hallysonh/ngx-imageviewer';
import 'hammerjs';
import { FileViewerComponent } from './file-viewer/file-viewer.component';
import { LoadingButtonComponent } from './loading-button/loading-button.component';
import { LoaderButtonComponent } from './loader-button/loader-button.component';
import { NavigationService } from './services/navigation.service';
import { FileUploadComponent } from './file-upload/file-upload.component';
import { LocationPickerComponent } from './location-picker/location-picker.component';
import { TreePickerComponent } from './tree-picker/tree-picker.component';
import { TreePickerService } from './services/tree-picker.service';
import { AlertService } from './services/alert.service';
import { ItemComponent } from './tree-picker/item/item.component';
import { CTranslateService } from './services/translate.service';

import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule,
    HttpModule,
    FormsModule,
    Daterangepicker,
    ImageViewerModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  exports: [
    HeaderComponent,
    SidebarComponent,
    SearchInputComponent,
    PaginationComponent,
    SortableComponent,
    LiveSearchComponent,
    DaterangepickerComponent,
    CheckboxGroupComponent,
    DatetimePickerComponent,
    DatepickerComponent,
    BreadcrumbComponent,
    LoaderComponent,
    WysiwygComponent,
    ImageUploadComponent,
    ImageViewComponent,
    FilePreviewComponent,
    SelectPickerComponent,
    LoadingButtonComponent,
    LoaderButtonComponent,
    DateFormatPipe,
    FileUploadComponent,
    LocationPickerComponent,
    TreePickerComponent
  ],
  providers: [
    customHttpProvider,
    RouteHelperService,
    ResourceService,
    NavigationService,
    TreePickerService,
    AlertService,
    CTranslateService
  ],
  declarations: [
    HeaderComponent,
    SidebarComponent,
    SearchInputComponent,
    PaginationComponent,
    LocationFilterInputComponent,
    AdminComponent,
    SortableComponent,
    LoaderComponent,
    NotFoundComponent,
    LiveSearchComponent,
    DaterangepickerComponent,
    CheckboxGroupComponent,
    DatetimePickerComponent,
    DatetimePickerDirective,
    DatepickerComponent,
    DatepickerDirective,
    DateFormatPipe,
    BreadcrumbComponent,
    WysiwygComponent,
    ImageUploadComponent,
    ImageViewComponent,
    FilePreviewComponent,
    SelectPickerComponent,
    FileViewerComponent,
    LoadingButtonComponent,
    LoaderButtonComponent,
    FileUploadComponent,
    LocationPickerComponent,
    TreePickerComponent,
    ItemComponent
  ]
})
export class CoreModule { }
