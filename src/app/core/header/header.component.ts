import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../auth/services/auth.service';
import { Router, NavigationEnd } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { NavigationService } from '../services/navigation.service';
import { TranslateService } from '@ngx-translate/core';
import { CTranslateService } from '../services/translate.service';

@Component({
  selector: 'ch-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  private show: boolean;

  constructor(
    private authService: AuthService,
    private router: Router,
    private navigationService: NavigationService,
    private translate: CTranslateService
  ) { }

  ngOnInit() {
    this.navigationService.showSidebar.subscribe((show: boolean) => {
      this.show = show;
    });
  }

  public goToDashboard() {
    this.router.navigateByUrl('/admin/dashboard');

    return false;
  }

  public editProfile() {
    this.router.navigateByUrl('/admin/profile');

    return false;
  }

  public changeLang() {
    this.translate.changeLanguage();

    return false;
  }

  public logout() {
    this.authService.logout().then(() => {
      this.router.navigateByUrl('/login');
    });

    return false;
  }

  public toggleSideBar() {
    this.navigationService.showSidebar.next(!this.show);
  }

  public get currentLang() {
    switch (this.translate.getCurrentLang()) {
      case 'en':
        return 'EN';
      case 'bg':
        return 'BG';
      default:
        return 'n/a';
    }
  }

}
