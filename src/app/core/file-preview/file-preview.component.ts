import { Component, Input, Output, OnInit, OnChanges, AfterViewChecked, ViewChild, EventEmitter } from '@angular/core';
import { IMAGEVIEWER_CONFIG, ImageViewerConfig } from '@hallysonh/ngx-imageviewer';
import { CHttp } from '../services/chttp.service';
import { config } from '../../app.config';
import { AuthDataStorageService } from '../../auth/services/auth-data-storage.service';

declare var jQuery: any;

const MY_IMAGEVIEWER_CONFIG: ImageViewerConfig = {
  width: 566,
  height: 477,
  bgStyle: '#fff',
  buttonStyle: {
    bgStyle: '#B71C1C'
  }
};

@Component({
  selector: 'ch-file-view',
  templateUrl: './file-preview.component.html',
  styleUrls: ['./file-preview.component.css'],
  providers: [
    {
      provide: IMAGEVIEWER_CONFIG,
      useValue: MY_IMAGEVIEWER_CONFIG
    }
  ]
})
export class FilePreviewComponent implements OnInit, OnChanges {
  @Input() public show: boolean;
  @Input() public data: any;
  @ViewChild('modal') private modal;
  public resourceId: string;
  public fileType: string;
  public loading: boolean;
  private change: boolean;
  private src: string;

  constructor(
    private http: CHttp,
    private authDataStorageService: AuthDataStorageService
  ) { }

  ngOnInit() { }

  ngOnChanges() {
    if (this.show) {
      this.change = true;
    }
    this.resourceId = this.data ? this.data.resourceId : '';
    const s = config.api_url + 'resources/' + this.resourceId + '?x-auth-token=' + this.authDataStorageService.getSessionTokenFromStorage();

    this.loading = true;
    this.http.get(s)
      .subscribe(
      resp => {
        this.fileType = this.isImage(resp.headers.get('Content-Type')) ? 'image' :
          (this.isPdf(resp.headers.get('Content-Type')) ? 'pdf' : 'image');
        this.src = s;

        this.loading = false;
      }
      );
  }

  private isImage(name) {
    return name.toLowerCase().match('\\.(png|jpg|jpeg|gif)|image/png') !== null;
  }

  private isPdf(name) {
    return name.toLowerCase().match('\\.(pdf)|application/pdf') !== null;
  }

  private getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

}
