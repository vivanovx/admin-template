import { Component, Input, Output, EventEmitter, OnInit, OnChanges } from '@angular/core';

@Component({
  selector: 'ch-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.css']
})
export class PaginationComponent implements OnInit, OnChanges {
  static readonly DEFAULT_PAGES_TO_SHOW = 9;

  @Output() pageChanged: EventEmitter<any> = new EventEmitter();
  @Input() public totalItems: number;
  @Input() public pageNumber: number;
  @Input() public pageSize: number;
  public totalPages: number;
  public pagesToShow: number = PaginationComponent.DEFAULT_PAGES_TO_SHOW;
  public pageItems: Array<number>;

  constructor() { }

  ngOnInit() {
    this.calculate();
  }

  ngOnChanges() {
    this.calculate();
  }

  private calculate() {
    this.totalPages = Math.ceil(+this.totalItems / this.pageSize);

    const pageRange = this.determinePagesToShow();
    this.pageItems = this.createRange(pageRange.start, pageRange.end);
  }

  private determinePagesToShow() {
    let result = { start: 1, end: this.pagesToShow };

    if (this.pagesToShow > this.totalPages) {
      result.end = this.totalPages;
    } else {
      const sideNumber = Math.floor(this.pagesToShow / 2);
      let start = this.pageNumber - sideNumber;
      let end = +this.pageNumber + sideNumber;
      if (1 > start) {
        end += Math.abs(1 - start);
        start = 1;
      } else if (this.totalPages < end) {
        start -= end - this.totalPages;
        end = this.totalPages;
      }

      result = { start, end };
    }

    return result;
  }

  private createRange(from: number, to: number) {
    const result = [];
    for (let i = from; i <= to; i++) {
      result.push(i);
    }

    return result;
  }

  public goToPage(page: number) {
    if (page === this.pageNumber) {
      return;
    }

    this.pageChanged.emit(page);
  }
}
