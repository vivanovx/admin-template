import { Component, Input, OnInit } from '@angular/core';
import { CHttp } from '../services/chttp.service';

@Component({
  selector: 'ch-file-viewer',
  templateUrl: './file-viewer.component.html',
  styleUrls: ['./file-viewer.component.css']
})
export class FileViewerComponent implements OnInit {
  @Input() public src: string;
  @Input() public filetype: string;

  constructor() { }

  ngOnInit() { }

}
