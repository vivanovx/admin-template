import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';

@Component({
  selector: 'ch-loading-button',
  templateUrl: './loading-button.component.html',
  styleUrls: ['./loading-button.component.css']
})
export class LoadingButtonComponent implements OnInit {
  @Input() public text: string;
  @Input() public className: string;
  @Input() public loading: boolean;
  @Input() public disabled: boolean;
  @Input() public size: number;
  @Output() public clicked: EventEmitter<any> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  public onClick(event) {
    this.clicked.emit();
  }

  get targetClass() {
    return ('string' === typeof this.className && '' !== this.className.trim()) ?
      this.className : 'btn btn-primary';
  }

}
