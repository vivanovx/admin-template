import {
  Component,
  Input,
  Output,
  EventEmitter,
  ViewChild,
  ElementRef,
  OnInit
} from '@angular/core';

declare var jQuery: any;

@Component({
  selector: 'ch-loader-button',
  templateUrl: './loading-button.component.html',
  styleUrls: ['./loading-button.component.css']
})
export class LoaderButtonComponent implements OnInit {
  @Input() public text: string;
  @Input() public className: string;
  @Input() public loading: boolean;
  @Input() public disabled: boolean;
  @Input() public size: number;
  @Output() public clicked: EventEmitter<any> = new EventEmitter();
  @ViewChild('button') public button: ElementRef;

  public buttonHeight: number;

  constructor() { }

  ngOnInit() {
  }

  public onClick(event) {
    this.buttonHeight = jQuery(this.button.nativeElement).height();
    this.clicked.emit();
  }

  get targetClass() {
    return ('string' === typeof this.className && '' !== this.className.trim()) ?
      this.className : 'btn btn-primary';
  }

}
