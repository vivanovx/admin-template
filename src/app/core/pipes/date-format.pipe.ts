import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

const TYPE_DATE = 'date';
const TYPE_DATETIME = 'datetime';

@Pipe({
  name: 'dateFormat'
})
export class DateFormatPipe implements PipeTransform {

  transform(value: string, type: string, format?: string): string {
    if (!value) {
      return '-';
    }

    return moment(new Date(value)).format(TYPE_DATETIME === type ? 'YYYY-MM-DD HH:mm' : ('cust' === type ? format : 'YYYY-MM-DD'));
  }

}
