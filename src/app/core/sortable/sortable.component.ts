import { Component, Input, Output, EventEmitter, OnInit, OnChanges } from '@angular/core';

@Component({
  selector: 'ch-sortable',
  templateUrl: './sortable.component.html',
  styleUrls: ['./sortable.component.css']
})
export class SortableComponent implements OnInit, OnChanges {
  public static readonly ORDER_ASC = 'ASC';
  public static readonly ORDER_DESC = 'DESC';

  @Input() public order: string;
  @Input() public column: string;
  @Input() public pageParams: any;
  @Output() public sort: EventEmitter<any> = new EventEmitter();

  constructor() { }

  ngOnInit() {
    this.setSortState();
  }

  ngOnChanges() {
    this.setSortState();
  }

  private setSortState() {
    if (this.column === this.pageParams.sortBy) {
      if ('ASC' !== this.pageParams.sortOrder && 'DESC' !== this.pageParams.sortOrder) {
        this.order = 'DESC';
      } else {
        this.order = this.pageParams.sortOrder;
      }
    } else {
      this.order = 'DESC';
    }
  }

  public onSort() {
    if (SortableComponent.ORDER_ASC === this.order) {
      this.sort.emit({ order: SortableComponent.ORDER_DESC, column: this.column });
    } else if (SortableComponent.ORDER_DESC === this.order) {
      this.sort.emit({ order: SortableComponent.ORDER_ASC, column: this.column });
    }
  }

}
