import { Component, Input, Output, EventEmitter, OnInit, OnChanges } from '@angular/core';

@Component({
  selector: 'ch-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.css']
})
export class BreadcrumbComponent implements OnInit, OnChanges {
  @Input() public parts = [];
  @Output() public goTo: EventEmitter<any> = new EventEmitter();
  public validatedParts: any[] = [];

  constructor() { }

  ngOnInit() {
  }

  ngOnChanges() {
    this.validatedParts = [];
    for (const item of this.parts || []) {
      if ('string' === typeof item && '' !== item.trim()) {
        this.validatedParts.push({ name: item });
      } else if (item && 'string' === typeof item.name && '' !== item.name.trim()) {
        this.validatedParts.push(item);
      }
    }
  }

  public goToLoc(data) {
    this.goTo.emit(data);

    return false;
  }

}
