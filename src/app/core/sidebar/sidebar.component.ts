import { Component, OnInit } from '@angular/core';
import { AuthDataStorageService } from '../../auth/services/auth-data-storage.service';
import { NavigationService } from '../services/navigation.service';
import { CTranslateService } from '../services/translate.service';

@Component({
  selector: 'ch-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  public showSidebar: boolean;
  public routes: Array<{ route?: string, label: string, badge?: number, hide?: boolean, subRoutes?: Array<{ route: string, label: string, hide?: boolean }> }>;

  constructor(
    private authDataStorageService: AuthDataStorageService,
    private navigationService: NavigationService,
    private translate: CTranslateService) {

    (async () => {
      this.routes = [
        {
          route: 'dashboard', label: await this.translate.getTranslatedText('dashboard:sidebar'),
          hide: this.isCustomerSupport
        },
        {
          route: 'clients', label: await this.translate.getTranslatedText('clients:sidebar')
        },
        {
          route: 'admin-users', label: await this.translate.getTranslatedText('admin-users:sidebar'),
          hide: this.isCustomerSupport
        },
        {
          label: await this.translate.getTranslatedText('reference-data:sidebar'),
          subRoutes: [
            {
              route: 'locations', label: await this.translate.getTranslatedText('locations:sidebar'),
              hide: this.isCustomerSupport
            },
            {
              route: 'pages', label: await this.translate.getTranslatedText('static-pages:sidebar'),
              hide: this.isCustomerSupport
            },
          ]
        },
      ];
    })();

    navigationService.showSidebar.subscribe((show: boolean) => {
      this.showSidebar = show;
    });
  }

  ngOnInit() {
  }

  public hasSubItems(item: any): boolean {
    if (item && item.subRoutes instanceof Array) {
      if (0 === item.subRoutes) {
        return false;
      }
      for (let subItem of item.subRoutes) {
        if (!subItem.hide) {
          return true;
        }
      }
    }

    return false;
  }

  get isCustomerSupport() {
    return this.authDataStorageService.isCustomerSupport();
  }

  get isDoctorMonitoring() {
    return this.authDataStorageService.isDoctorMonitoring();
  }

}
