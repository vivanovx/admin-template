import { Directive, Input, Output, ElementRef, EventEmitter, SimpleChanges, OnInit } from '@angular/core';
import * as moment from 'moment';

declare var jQuery: any;

@Directive({
  selector: '[chDatetimepicker]'
})
export class DatetimePickerDirective implements OnInit {
  @Input() public value: string;
  @Output() changed: EventEmitter<any> = new EventEmitter();
  private ele: ElementRef;
  private date: any;

  private dataFormat = 'LT';

  constructor(ele: ElementRef) {
    this.ele = ele;
  }

  ngOnInit() {
    jQuery(this.ele.nativeElement)['datetimepicker']({
      format: this.dataFormat
    })
      .on('dp.change', (e) => {
        if (e.date !== this.date) {
          this.date = e.date;
          this.changed.emit(e.date);
        }
      });
  }

}
