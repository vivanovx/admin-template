
import { Component, OnInit, OnChanges, Input, Output, EventEmitter, SimpleChanges } from '@angular/core';
import * as moment from 'moment';

@Component({
  selector: 'ch-datetime-picker',
  templateUrl: './datetime-picker.component.html',
  styleUrls: ['./datetime-picker.component.css']
})
export class DatetimePickerComponent implements OnInit, OnChanges {
  @Input() public value: string;
  @Output() public changed: EventEmitter<any> = new EventEmitter();

  private dateFormat = 'LT';

  constructor() { }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.value && changes.value.currentValue) {
      this.value = moment(this.value, this.dateFormat).format(this.dateFormat);
    }
  }

  public change(value) {
    this.changed.emit(value);
  }

}
