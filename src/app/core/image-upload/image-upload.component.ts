import {
  Component,
  Input,
  Output,
  ViewChild,
  OnInit,
  EventEmitter,
  ElementRef,
  Renderer2
} from '@angular/core';
import { RequestOptions, Headers, Http } from '@angular/http';
import { CHttp } from '../../core/services/chttp.service';
import { ResourceService } from '../services/resource.service';
import { AuthDataStorageService } from '../../auth/services/auth-data-storage.service';
import jquery from 'jquery';
import swal from 'sweetalert2';

@Component({
  selector: 'ch-image-upload',
  templateUrl: './image-upload.component.html',
  styleUrls: ['./image-upload.component.css']
})
export class ImageUploadComponent implements OnInit {
  @Input() public resourceId: string;
  @Input() public maxWidth: number;
  @Input() public maxHeight: number;

  @Output() public changed: EventEmitter<string> = new EventEmitter();
  @ViewChild('inp') private fileInp: ElementRef;
  private endPoint: string;
  private token: string;

  constructor(
    private http: CHttp,
    private rd: Renderer2,
    private resouceService: ResourceService,
    private authDataStorageService: AuthDataStorageService
  ) {
    this.token = this.authDataStorageService.getSessionTokenFromStorage();
  }

  ngOnInit() { }

  fileChange(event) {
    const _URL = window.URL || window['webkitURL'];
    const file = this.fileInp.nativeElement.files[0];
    if (file) {
      const img = new Image();
      img.onload = () => {
        if ((!this.maxWidth || img.width === this.maxWidth) &&
          (!this.maxHeight || img.height === this.maxHeight)
        ) {
          this.doUpload(event);
        } else {
          swal('Upload failed', `Image size must be ${this.maxWidth}x${this.maxHeight}`, 'error');
        }
      };
      img.src = _URL.createObjectURL(file);
    }
  }

  private doUpload(files) {
    this.resouceService.uploadFile(files)
      .subscribe(
        data => {
          this.resourceId = data.resourceId;
          this.changed.emit(data.resourceId);
        },
        error => console.log(error)
      );
  }

  public triggerUpload() {
    this.fileInp.nativeElement.click();
  }

}
