import { Component, Input, Output, EventEmitter, OnInit, OnChanges, AfterViewInit, ViewChild, ElementRef } from '@angular/core';

declare var jQuery: any;

@Component({
  selector: 'ch-wysiwyg',
  templateUrl: './wysiwyg.component.html',
  styleUrls: ['./wysiwyg.component.css']
})
export class WysiwygComponent implements OnInit, OnChanges, AfterViewInit {
  @Output() changed: EventEmitter<any> = new EventEmitter();
  @Input() public value: any = 'ddd <b>ddd x</b>';
  @ViewChild('chWysiwyg') public ele: ElementRef;
  private $ele;

  constructor() { }

  ngOnInit() {
  }

  public ngAfterViewInit() {
    this.$ele = jQuery(this.ele.nativeElement);
    this.$ele['summernote']({
      toolbar: [
        ['style', ['bold', 'italic', 'underline', 'clear']],
        ['font', ['strikethrough', 'superscript', 'subscript']],
        ['fontsize', ['fontsize']],
        ['fontname', ['fontname']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['height', ['height']],
        ['view', ['codeview']]
      ],
      fontSize: 11,
      // fontName: 'Verdana'
      fontName: 'Verdana, Arial, Helvetica, sans-serif'
    });
    this.$ele['summernote']('code', this.value);
    this.$ele.on('summernote.change', (we, contents, $editable) => {
      // document.getElementById('d1').innerHTML = '<div style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px;">' + contents + '</div>';
      this.changed.emit(contents);
    });
  }

  public ngOnChanges(changes) {
    if (changes.value && changes.value.currentValue && this.$ele) {
      this.$ele['summernote']('code', this.value);
    }
  }

}
