import { Component, Input, Output, OnInit, OnChanges, AfterViewInit, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { config } from '../../app.config';
import { AuthDataStorageService } from '../../auth/services/auth-data-storage.service';

declare var mediumZoom: any;

@Component({
  selector: 'ch-image-view',
  templateUrl: './image-view.component.html',
  styleUrls: ['./image-view.component.css']
})
export class ImageViewComponent implements OnInit, OnChanges, AfterViewInit {
  @Output() public clicked: EventEmitter<any> = new EventEmitter();
  @Input() public resourceId: string;
  @Input() public zoom = false;
  @ViewChild('image') public image: ElementRef;
  public loading = false;
  private endPoint: string;
  private token: string;
  private apiEndpoint: string;

  constructor(private authDataStorageService: AuthDataStorageService) {
    this.token = this.authDataStorageService.getSessionTokenFromStorage();
    this.apiEndpoint = config.api_url;
  }

  ngOnInit() {
    if (this.zoom) {
      mediumZoom(this.image.nativeElement, {
        background: '#131711'
      });
    }
  }

  ngOnChanges(data) {
    if ('object' === typeof data.resourceId &&
      data.resourceId.currentValue !== data.resourceId.previousValue &&
      'string' === typeof data.resourceId.currentValue &&
      '' !== data.resourceId.currentValue.trim()
    ) {
      this.loading = true;
    }
  }

  ngAfterViewInit() {
    this.image.nativeElement.onload = () => {
      this.loading = false;
    };
  }

  public onClick(event) {
    this.clicked.emit(event);
  }

  get hasNoImage() {
    return !this.resourceId && !this.loading;
  }

  get imageUrl() {
    return this.resourceId ?
      (this.apiEndpoint + 'resources/' + this.resourceId + '?x-auth-token=' + this.token) :
      '';
  }

}
