import { Component, Input, Output, EventEmitter, OnInit, OnChanges, AfterViewInit, AfterViewChecked, ViewChild, ElementRef } from '@angular/core';

declare var jQuery: any;

@Component({
  selector: 'ch-live-search',
  templateUrl: './live-search.component.html',
  styleUrls: ['./live-search.component.css']
})
export class LiveSearchComponent implements OnInit, OnChanges, AfterViewInit, AfterViewChecked {
  @Input() public value: string;
  @Input() items: Array<any> = [];
  @Input() showClass: boolean;
  @Output() change = new EventEmitter<any>();
  @ViewChild('inp') public inp: ElementRef;
  private sync: boolean;

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.syncSelectPicker();
  }

  ngAfterViewChecked() {
    if (this.sync) {
      this.syncSelectPicker();
    }
  }

  ngOnChanges(data) {
    if (data) {
      this.sync = true;
    }
  }

  private syncSelectPicker() {
    if ('undefined' === typeof jQuery) {
      return;
    }

    jQuery(this.inp.nativeElement)['selectpicker']('refresh');
    this.sync = false;
  }

  public itemSelected(event) {
    this.change.emit(event.target.value);
  }
}
