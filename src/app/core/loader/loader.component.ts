import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'ch-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.css']
})
export class LoaderComponent implements OnInit {
  @Input() public loading: boolean;
  @Input() public listingPage: boolean;

  constructor() { }

  ngOnInit() {
  }

}
