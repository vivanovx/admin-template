import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core';

@Component({
  selector: 'ch-checkbox-group',
  templateUrl: './checkbox-group.component.html',
  styleUrls: ['./checkbox-group.component.css']
})
export class CheckboxGroupComponent implements OnInit {
  @Input() public value: string;
  @Input() public multiselect = false;
  @Input() public values: string[] = [];
  @Output() public selected: EventEmitter<any> = new EventEmitter();

  public checkboxValues = [
    { value: 'Sun' }, { value: 'Mon' }, { value: 'Tue' }, { value: 'Wed' }, { value: 'Thu' }, { value: 'Fri' }, { value: 'Sat' }
  ];

  constructor() { }

  ngOnInit() {
    if (!(this.values instanceof Array)) {
      this.values = [];
    }
  }

  public checkboxChange(index, targetElement) {
    const checked = targetElement.checked;
    for (const i of Object.keys(this.checkboxValues)) {
      if (i !== index) {
        continue;
      }

      const cValue = this.checkboxValues[i].value;
      if (!checked) {
        this.values.splice(this.values.indexOf(cValue), 1);

        continue;
      }

      if (!this.containsValue(cValue)) {
        this.values.push(cValue);
      }
    }

    this.selected.emit({
      value: this.checkboxValues[index].value,
      checked: checked,
      allSelected: this.values
    });
  }

  public isChecked(value) {
    return this.containsValue(value);
  }

  private containsValue(value) {
    return 0 <= this.values.indexOf(value);
  }

}
