import { Injectable } from '@angular/core';
import * as alertify from 'alertify.js';

enum AlertPosition {
  TOP_LEFT = 'TOP_LEFT',
  TOP_RIGHT = 'TOP_RIGHT',
  BOTTOM_LEFT = 'BOTTOM_LEFT',
  BOTTOM_RIGHT = 'BOTTOM_RIGHT',
}

@Injectable()
export class AlertService {

  constructor() {
    this.setPosition(AlertPosition.BOTTOM_RIGHT);
  }

  public setPosition(target: AlertPosition) {
    let position;
    switch (target) {
      case AlertPosition.TOP_LEFT:
        position = 'top left';
        break;
      case AlertPosition.TOP_RIGHT:
        position = 'top right';
        break;
      case AlertPosition.BOTTOM_LEFT:
        position = 'bottom left';
        break;
      case AlertPosition.BOTTOM_RIGHT:
        position = 'bottom right';
        break;
      default:
        return;
    }
    alertify.logPosition(position);
  }

  public error(message: string) {
    alertify.error(message);
  }

  public success(message: string) {
    alertify.success(message);
  }

}
