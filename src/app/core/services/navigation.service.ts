import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class NavigationService {

  public showSidebar: Subject<boolean> = new Subject();

  constructor() { }

}
