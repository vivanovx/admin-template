import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Injectable()
export class CTranslateService {
  private curLang: string;

  constructor(
    private translate: TranslateService
  ) { }

  public getTranslatedText(key: string, data?: any) {
    return this.translate.get(key, data)
      .toPromise();
  }

  public setLanguage() {
    const curLang = localStorage.getItem('lang');
    const browserLang = this.translate.getBrowserLang();
    this.curLang = curLang || browserLang || 'en';

    this.translate.setDefaultLang(this.curLang);
  }

  public changeLanguage() {
    const curLang = this.getCurrentLang();
    let targetLang = 'en';
    if ('en' === curLang) {
      targetLang = 'bg';
    }
    localStorage.setItem('lang', targetLang);
    window.location.reload();
  }

  public getCurrentLang() {
    const curLang = this.translate.currentLang || this.curLang;

    return curLang;
  }

  public getBase() {
    return this.translate;
  }

}
