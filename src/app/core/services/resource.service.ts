import { Injectable } from '@angular/core';
import { RequestOptions, Headers, Http } from '@angular/http';
import { config } from '../../app.config';
import { CHttp } from './chttp.service';

@Injectable()
export class ResourceService {

  constructor(private http: CHttp) { }

  public uploadFile(event: any) {
    const fileList: FileList = event.target.files;

    const file = fileList[0];

    const formData = new FormData();
    formData.append('file', file, file.name);

    return this.http.post(config.api_url + 'resources', formData)
      .map(res => res.json());
  }

}
