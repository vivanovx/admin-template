import { Injectable } from '@angular/core';
import { SortableComponent } from '../sortable/sortable.component';
import { Router } from '@angular/router';

@Injectable()
export class RouteHelperService {

  constructor(private router: Router) { }

  public isCurrentPageEdit() {
    const urlParts = this.getUrlParts();

    return 'add' !== urlParts[urlParts.length - 1];
  }

  public getUrlParts() {
    return this.filterNonEmpty(this.getUrl().split('/'));
  }

  public getUrl() {
    const a = this.filterNonEmpty(this.router.url.split('?'));
    return 0 < a.length ? a[0] : '';
  }

  private filterNonEmpty(a: string[]) {
    const result = [];
    for (const i of a) {
      if ('string' !== typeof i || '' === i.trim()) {
        continue;
      }

      result.push(i);
    }

    return result;
  }

  public getValidatedParams(params) {
    const result = {};

    for (const param of Object.keys(params)) {
      result[param] = params[param];
    }
    if (!this.isValidPositiveInt(result['pageNumber'])) {
      result['pageNumber'] = 1;
    }
    if (!this.isValidPositiveInt(result['pageSize'])) {
      result['pageSize'] = 10;
    }

    return result;
  }

  public queryParams(targetParams: any, curPageParams: any) {
    const result = JSON.parse(JSON.stringify(curPageParams));

    for (const name in targetParams) {
      if ('pageNumber' === name && undefined !== targetParams.pageNumber) {
        if (!this.isValidPositiveInt(targetParams.pageNumber)) {
          result.pageNumber = this.isValidPositiveInt(result.pageNumber) ? result.pageNumber : 1;
        } else {
          result.pageNumber = targetParams.pageNumber;
        }
      } else if ('pageSize' === name && undefined !== targetParams.pageSize) {
        if (!this.isValidPositiveInt(targetParams.pageSize)) {
          result.pageSize = this.isValidPositiveInt(result.pageSize) ? result.pageSize : 10;
        } else {
          result.pageSize = targetParams.pageSize;
        }
      } else if ('sortOrder' === name && undefined !== targetParams.sortOrder) {
        result.sortOrder = (SortableComponent.ORDER_ASC !== targetParams.sortOrder && SortableComponent.ORDER_DESC !== targetParams.sortOrder) ?
          SortableComponent.ORDER_ASC :
          targetParams.sortOrder;
      } else if (targetParams[name]) {
        result[name] = targetParams[name];
      }
    }

    return result;
  }

  private isValidPositiveInt(n: any) {
    return this.isNumeric(n) && 1 <= n;
  }

  private isNumeric(n: any) {
    return isFinite(Number.parseInt(n));
  }

  public f(url: string) {
    this.router.navigateByUrl(url);
  }

  public buildQueryString(data, except: string[] = []) {
    const result = [];
    for (const i of Object.keys(data)) {
      let b = false;
      for (const j in except) {
        if (except[j] === i) {
          b = true;
          break;
        }
      }
      if (b) {
        continue;
      }

      if (data.hasOwnProperty(i)) {
        result.push(encodeURIComponent(i) + '=' + encodeURIComponent(data[i]));
      }
    }

    return result.join('&');
  }

}
