import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Subscription } from 'rxjs/Subscription';

@Injectable()
export class TreePickerService {

  private itemsSubject$: Subject<any>;
  private selectedSubject$: Subject<any>;

  constructor() {
    this.itemsSubject$ = new Subject();
    this.selectedSubject$ = new Subject();
  }

  public setItems(data: any) {
    this.itemsSubject$.next(data);
  }

  public onItemsSet(success, error?, compelete?): Subscription {
    return this.itemsSubject$.subscribe(success, error, compelete);
  }

  public setSelectedItems(data: any) {
    this.selectedSubject$.next(data);
  }

  public onSelectedItems(success, error?, compelete?): Subscription {
    return this.selectedSubject$.subscribe(success, error, compelete);
  }

}
