import { Component, Input, Output, OnInit, OnChanges, AfterViewInit, AfterViewChecked, EventEmitter, ViewChild, ElementRef } from '@angular/core';

declare var jQuery: any;

@Component({
  selector: 'ch-select-picker',
  templateUrl: './select-picker.component.html',
  styleUrls: ['./select-picker.component.css']
})
export class SelectPickerComponent implements OnInit, OnChanges, AfterViewInit, AfterViewChecked {

  @Input() public value: string;
  @Input() public items: Array<any> = [];
  @Input() public showClass: boolean;
  @Output() public changed: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild('select') public select: ElementRef;
  private sync: boolean;

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.syncSelectPicker();
  }

  ngAfterViewChecked() {
    if (this.sync) {
      this.syncSelectPicker();
    }
  }

  ngOnChanges(data) {
    if (data) {
      this.sync = true;
    }
  }

  private syncSelectPicker() {
    if ('undefined' === typeof jQuery) {
      return;
    }

    jQuery(this.select.nativeElement)['selectpicker']('refresh');
    this.sync = false;
  }

  public itemSelected(event) {
    this.changed.emit(event);
  }

}
