export class BaseComponent {
  /**
   * page loading
   */
  public loading: boolean;

  /**
   * page loaded
   */
  public loaded: boolean;

  /**
   * translation map
   */
  public texts: any = {};

}

export class BaseEditComponent extends BaseComponent {
  public pageLoading: boolean;
  public formLoading: boolean;
  /**
   * Is the current page edit or create.
   */
  public editMode: boolean;

  get isInitialLoading() {
    return this.pageLoading || (this.loading && !this.loaded);
  }

  protected setFormLoading(loading: boolean) {
    this.formLoading = loading;
  }

}
