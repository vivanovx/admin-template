import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'ch-tree-picker-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})
export class ItemComponent implements OnInit {

  @Input() public item;
  @Output() public selected: EventEmitter<any> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  public check1(id: string, e) {
    this.checkItem(id, e);

    return false;
  }

  private checkItem(id: string, ch: boolean) {
    this.chItem(this.item, id, ch);
  }

  private chItem(o: any, id: string, ch: boolean) {
    if (o.id === id) {
      o.marked = ch;

      return true;
    }

    return false;
  }

  public onCheck(id: string, event) {
    event.preventDefault();
    this.selected.emit({ id, checked: event.target.checked });
  }

  public onSelected(data: { id: string, checked: boolean }) {
    this.selected.emit(data);
  }

  get hasChildren() {
    return this.item.children instanceof Array && 0 < this.item.children.length;
  }

}
