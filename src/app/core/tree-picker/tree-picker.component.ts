import { Component, Input, Output, EventEmitter, OnInit, OnChanges } from '@angular/core';
import { TreePickerService } from '../services/tree-picker.service';

@Component({
  selector: 'ch-tree-picker',
  templateUrl: './tree-picker.component.html',
  styleUrls: ['./tree-picker.component.css']
})
export class TreePickerComponent implements OnInit {

  @Input() public listItems: any[];
  @Input() public selected: any[] = [];

  @Output() public itemCheck: EventEmitter<any> = new EventEmitter();
  @Output() public itemUncheck: EventEmitter<any> = new EventEmitter();
  @Output() public topLevelSelected: EventEmitter<string[]> = new EventEmitter();

  public items: any;
  private selectedTmp: any[];

  constructor(private treePickerService: TreePickerService) { }

  ngOnInit() {
    this.treePickerService.onItemsSet(result => {
      this.listItems = result;
      this.items = this.getNodes() || [];
    });
    this.treePickerService.onSelectedItems(selected => {
      this.selected = selected;
      this.setSelected(this.selected);
    });
  }

  private setSelected(ids: string[]) {
    for (const id of ids) {
      this.selectItem(id, true, false);
    }

    this.topLevelSelected.emit(this.getTopLevelSelected());
  }

  private getNodes() {
    const nodes = this.buildItems(this.getCountries());

    return nodes;
  }

  private buildItems(items: any[]) {
    const nodes: any[] = [];
    for (const item of items) {
      const node: any = { id: item.id, name: item.nameBg, isTopLevel: item.isTopLevel };
      const subItems = this.getSubitems(item.id);
      if (subItems.length) {
        node.children = [];
      }
      node.children = this.buildItems(subItems);

      nodes.push(node);
    }

    return nodes;
  }

  public getCountries() {
    const result = [];
    for (const item of this.listItems) {
      if (item.isTopLevel) {
        result.push(item);
      }
    }

    return result;
  }

  public getSubitems(id: string) {
    const result = [];
    for (const item of this.listItems) {
      if (item.serviceCategoryId === id) {
        result.push(item);
      }
    }

    return result;
  }

  public checkLocation(item, checked) {
    item.checked = checked;
  }

  public onSelected(data: { id: string, checked: boolean }) {
    return this.checkItem(data.id, data.checked);
  }

  public ch(id: string, e) {
    e.preventDefault();
    return this.checkItem(id, e.target.checked);
  }

  private checkItem(id: string, checked: boolean) {
    this.selectItem(id, checked, true);
    const topLevelSelected = this.getTopLevelSelected();
    this.topLevelSelected.emit(topLevelSelected);

    return false;
  }

  private disableSubchildren(id: string, b: boolean) {
    const item = this.findItem(id);

    this.disableCheck(item.children || [], b);
  }

  private disableCheck(items: any[], check: boolean) {
    for (const item of items) {
      item.disabled = check;
      item._checked = check;

      const hasChildren = item.children instanceof Array && 0 < item.children.length;
      if (hasChildren) {
        if (!check && item.checked) {
          return;
        }

        this.disableCheck(item.children, check);
      }
    }
  }

  private findItem(id: string) {
    const result = this.getItem(this.items, id);

    return result;
  }

  private getItem(items: any[], id: string) {
    for (const item of items || []) {
      if (item.id === id) { return item; }
      const found = this.getItem(item.children, id);
      if (found) { return found; }
    }

    return null;
  }

  private selectItem(id: string, ch: boolean, emit: boolean = true) {
    this.selectItemR({ items: this.items, id, ch, emit });
  }

  private selectItemR(data: any) {
    const items = data.items || [];
    for (const item of items) {
      const tData = {
        item,
        id: data.id,
        ch: data.ch,
        emit: data.emit
      };
      if (this.selectItem1(tData)) {
        return;
      }

      data.items = item.children;
      this.selectItemR(data);
    }
  }

  private selectItem1(data: any) {
    if (data.item.id === data.id) {
      data.item.checked = data.ch;
      this.disableSubchildren(data.id, data.ch);
      if (data.emit) {
        if (data.ch) {
          this.itemCheck.emit(data.id);
        } else {
          this.itemUncheck.emit(data.id);
        }
      }

      return true;
    }

    return false;
  }

  private getTopLevelSelected() {
    const topLevelSelected = this.getSelected(this.items);

    return topLevelSelected;
  }

  private getSelected(items: any[]) {
    let result: string[] = [];
    for (const item of items || []) {
      if (item.checked) {
        result.push(item.id);
      } else {
        result = result.concat(this.getSelected(item.children));
      }
    }

    return result;
  }

  private checkFilterNested(o: any, s: string) {
    if (this.countains(o.name, s)) {
      return true;
    }
    const filterChildren = this.checkFilterChildren(o, s);
    o.noChildren = !filterChildren;

    return filterChildren;
  }

  private checkFilterChildren(o: any, s: string) {
    for (const item of o.children || []) {
      if (this.checkFilterNested(item, s)) {
        return true;
      }
    }

    return false;
  }

  private countains(a: string, t: string) {
    if (a.indexOf(t) > -1) {
      return true;
    }

    return false;
  }

}
