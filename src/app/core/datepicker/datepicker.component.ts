import {
  Component,
  ElementRef,
  OnInit,
  OnChanges,
  Input,
  Output,
  EventEmitter,
  ViewChild
} from '@angular/core';
import * as moment from 'moment';

declare var jQuery: any;

@Component({
  selector: 'ch-datepicker',
  templateUrl: './datepicker.component.html',
  styleUrls: ['./datepicker.component.css']
})
export class DatepickerComponent implements OnInit, OnChanges {
  @Input() public date: string;
  @Output() public changed: EventEmitter<any> = new EventEmitter();
  @ViewChild('input') private input: ElementRef;
  public value: string;
  private dateFormat = 'MM-DD-YYYY';

  constructor() { }

  ngOnInit() {
  }

  public change(value) {
    this.changed.emit(value);
  }

  ngOnChanges() {
    if (!isNaN(new Date(this.date).getTime())) {
      this.value = moment(this.date).format(this.dateFormat);
    }
  }

  public changeInputValue() {
    const value = this.input.nativeElement.value;
    const targetValue = this.isValidDate(value) ? moment(this.date) : null;
    this.changed.emit(targetValue);
  }

  private isValidDate(data: string): boolean {
    return !isNaN(new Date(data).getTime());
  }

}
