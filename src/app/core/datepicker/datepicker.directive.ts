import {
  Directive,
  Input,
  Output,
  ElementRef,
  EventEmitter,
  AfterViewInit
} from '@angular/core';
import * as moment from 'moment';

declare var jQuery: any;

@Directive({
  selector: '[chDatepicker]'
})
export class DatepickerDirective implements AfterViewInit {

  private dateFormat = 'MM-DD-YYYY';

  @Output() changed: EventEmitter<any> = new EventEmitter();
  @Input() public date: string;
  private value: string;

  private $ele;

  constructor(
    private ele: ElementRef
  ) { }

  ngAfterViewInit() {
    this.$ele = jQuery(this.ele.nativeElement);
    this.$ele['datetimepicker']({
      format: this.dateFormat,
    })
      .on('dp.change', (e) => {
        if (e.date !== this.date) {
          this.date = e.date;
          this.changed.emit(e.date);
        }
      });
  }

}
