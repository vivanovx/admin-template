import { Component, Input, Output, OnInit, OnChanges, AfterViewInit, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { Daterangepicker, DaterangepickerConfig } from 'ng2-daterangepicker';
import * as moment from 'moment';

declare var jQuery: any;

@Component({
  selector: 'ch-daterangepicker',
  templateUrl: './daterangepicker.component.html',
  styleUrls: ['./daterangepicker.component.css']
})
export class DaterangepickerComponent implements OnInit, OnChanges, AfterViewInit {
  @Input() public data: any;
  @Input() public pastSearch: any;
  @Output() public selected: EventEmitter<any> = new EventEmitter();
  @ViewChild('inp') public input: ElementRef;

  public dateInputs: any = [
    {
      start: moment().subtract(12, 'month'),
      end: moment().subtract(6, 'month')
    },
    {
      start: moment().subtract(9, 'month'),
      end: moment().subtract(6, 'month')
    },
    {
      start: moment().subtract(4, 'month'),
      end: moment()
    },
    {
      start: moment(),
      end: moment().add(5, 'month'),
    }
  ];

  public mainInput = {
    start: null,
    end: null
  };
  public label: string;

  constructor(private daterangepickerOptions: DaterangepickerConfig) {
    this.daterangepickerOptions.settings = {
      locale: { format: 'YYYY-MM-DD' },
      alwaysShowCalendars: false,
      ranges: {
        '1 week': [moment(), moment().add(1, 'week')],
        '2 weeks': [moment(), moment().add(2, 'week')],
        '1 month': [moment(), moment().add(1, 'month')],
        '3 months': [moment(), moment().add(3, 'month')],
        '1 year': [moment(), moment().add(1, 'year')],
      }
    };
  }

  ngOnInit() {
    if (this.data) {
      this.mainInput.start = this.data.startDate || this.mainInput.start;
      this.mainInput.end = this.data.endDate || this.mainInput.end;
    }
    if ('true' === this.pastSearch) {
      this.daterangepickerOptions.settings.ranges = {
        '1 week': [moment().subtract(1, 'week'), moment()],
        '2 weeks': [moment().subtract(2, 'week'), moment()],
        '1 month': [moment().subtract(1, 'month'), moment()],
        '3 months': [moment().subtract(3, 'month'), moment()],
        '1 year': [moment().subtract(1, 'year'), moment()],
      };
    } else {
      this.daterangepickerOptions.settings.ranges = {
        '1 week': [moment(), moment().add(1, 'week')],
        '2 weeks': [moment(), moment().add(2, 'week')],
        '1 month': [moment(), moment().add(1, 'month')],
        '3 months': [moment(), moment().add(3, 'month')],
        '1 year': [moment(), moment().add(1, 'year')],
      };
    }
  }

  ngOnChanges(changes) {
    if (changes.data && changes.data.previousValue !== changes.data.currentValue) {
      this.mainInput.start = ('' === this.data.startDate || this.data.startDate) ? this.data.startDate : this.mainInput.start;
      this.mainInput.end = ('' === this.data.endDate || this.data.endDate) ? this.data.endDate : this.mainInput.end;
    }
  }

  ngAfterViewInit() {
    const ele = this.input.nativeElement;
    jQuery(ele).daterangepicker(this.daterangepickerOptions.settings);

    jQuery(ele).on('apply.daterangepicker', (ev, picker) => {
      this.mainInput.start = picker.startDate;
      this.mainInput.end = picker.endDate;

      this.selected.emit({
        start: picker.startDate,
        end: picker.endDate
      });
      jQuery(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
    });

    jQuery(ele).on('cancel.daterangepicker', function (ev, picker) {
      jQuery(this).val('');
    });
  }

  public selectedDate(value: any, dateInput: any) {
    dateInput.start = value.start;
    dateInput.end = value.end;

    this.label = value.label;
    this.selected.emit(value);
  }

  public applyDate(event) {
  }

  public calendarEventsHandler(e: any) {
  }

}
