import { Routes, RouterModule } from '@angular/router';
import { EditAdminUserComponent } from './admin-users/edit-admin-user/edit-admin-user.component';
import { AdminUsersListingComponent } from './admin-users/admin-users-listing/admin-users-listing.component';
import { PagesListingComponent } from './pages/pages-listing/pages-listing.component';
import { PageEditComponent } from './pages/page-edit/page-edit.component';
import { LocationListingComponent } from './locations/location-listing/location-listing.component';
import { LocationEditComponent } from './locations/location-edit/location-edit.component';
import { DashboardComponent } from './dashboard/dashboard/dashboard.component';
import { LoginComponent } from './auth/login/login.component';
import { ForgottenPasswordComponent } from './auth/forgotten-password/forgotten-password.component';
import { AdminComponent } from './core/admin/admin.component';
import { AuthGuard } from './auth/guard/auth.guard';
import { LoggedInGuard } from './auth/guard/logged.guard';
import { NotFoundComponent } from './core/not-found/not-found.component';
import { ProfileEditComponent } from './admin-users/edit-admin-user/profile-edit.component';
import { SuperUserRequiredGuard } from './auth/guard/super-user-required.guard';
import { ClientGuard } from './auth/guard/client.guard';
import { BlankComponent } from './blank/blank.component';
import { ClientListingComponent } from './clients/client-listing/client-listing.component';
import { ClientDetailsComponent } from './clients/client-details/client-details.component';

const appRoutes: Routes = [
  {
    path: 'login',
    component: LoginComponent,
    canActivate: [LoggedInGuard]
  },
  {
    path: 'forgotten-password',
    component: ForgottenPasswordComponent,
    canActivate: [LoggedInGuard]
  },
  {
    path: 'not-found',
    component: NotFoundComponent
  },
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  },
  {
    path: 'admin',
    component: AdminComponent,
    children: [
      {
        path: 'dashboard',
        component: DashboardComponent,
        canActivate: [SuperUserRequiredGuard]
      },
      {
        path: 'profile',
        component: ProfileEditComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'clients/view',
        component: ClientDetailsComponent,
        canActivate: [ClientGuard]
      },
      {
        path: 'clients',
        component: ClientListingComponent,
        canActivate: [ClientGuard]
      },
      {
        path: 'admin-users/edit',
        component: EditAdminUserComponent,
        canActivate: [SuperUserRequiredGuard]
      },
      {
        path: 'admin-users/add',
        component: EditAdminUserComponent,
        canActivate: [SuperUserRequiredGuard]
      },
      {
        path: 'admin-users',
        component: AdminUsersListingComponent,
        canActivate: [SuperUserRequiredGuard]
      },
      {
        path: 'pages/edit',
        component: PageEditComponent,
        canActivate: [SuperUserRequiredGuard]
      },
      {
        path: 'pages/add',
        component: PageEditComponent,
        canActivate: [SuperUserRequiredGuard]
      },
      {
        path: 'pages',
        component: PagesListingComponent,
        canActivate: [SuperUserRequiredGuard]
      },
      {
        path: 'locations/edit',
        component: LocationEditComponent,
        canActivate: [SuperUserRequiredGuard]
      },
      {
        path: 'locations/add',
        component: LocationEditComponent,
        canActivate: [SuperUserRequiredGuard]
      },
      {
        path: 'locations',
        component: LocationListingComponent,
        canActivate: [SuperUserRequiredGuard]
      },
      {
        path: '**',
        redirectTo: 'dashboard'
      },
    ],
  },
  {
    path: '**',
    redirectTo: 'not-found'
  }
];

export const routing = RouterModule.forRoot(appRoutes, {
  // useHash: true
});
