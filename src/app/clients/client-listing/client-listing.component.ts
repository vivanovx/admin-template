import { Component, OnInit } from '@angular/core';
import { ClientService } from '../services/client.service';

import { Router, ActivatedRoute, Params } from '@angular/router';
import { SortableComponent } from '../../core/sortable/sortable.component';
import { RouteHelperService } from '../../core/services/route-helper.service';
import { LocationService } from '../../locations/services/location.service';
import { config } from '../../app.config';
import { AuthDataStorageService } from '../../auth/services/auth-data-storage.service';
import * as moment from 'moment';
import { CTranslateService } from '../../core/services/translate.service';
import { BaseComponent } from '../../core/common/base-component';
import { AlertService } from '../../core/services/alert.service';

@Component({
  selector: 'ch-client-listing',
  templateUrl: './client-listing.component.html',
  styleUrls: ['./client-listing.component.css']
})
export class ClientListingComponent extends BaseComponent implements OnInit {

  public items = [];
  public totalItems;
  public pageParams: any = {};
  private filterFields: string[] = [
    'search'
  ];
  public token: any;
  public itemLoading: any = {};
  public itemBlocking: any = {};

  constructor(
    private alertService: AlertService,
    private clientService: ClientService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private routeHelperService: RouteHelperService,
    private authDataStorageService: AuthDataStorageService,
    private translate: CTranslateService
  ) {
    super();
    this.token = this.authDataStorageService.getSessionTokenFromStorage();

    (async () => {
      this.texts.preview = await this.translate.getTranslatedText('preview:listing');
      this.texts.block = await this.translate.getTranslatedText('block:listing');
      this.texts.unblock = await this.translate.getTranslatedText('unblock:listing');
      this.texts.resetPassword = await this.translate.getTranslatedText('reset-password:listing');
    })();
  }

  ngOnInit() {
    this.activatedRoute.queryParamMap
      .map((params: Params) => params.params)
      .subscribe((params) => {
        this.pageParams = this.routeHelperService.getValidatedParams(params);
        this.pageParams = this.getFilteredPageParams(this.pageParams);
        this.loadItems(this.pageParams);
      });
  }

  private getFilteredPageParams(params: any) {
    const result = { ...params };
    if (params && !params.sortBy) {
      result.sortBy = 'SIGNED_UP';
      result.sortOrder = 'DESC';
    }

    return result;
  }

  public viewClient(id: string) {
    this.router.navigate(['/admin/clients/view'], {
      queryParams: {
        id: id
      }
    });
  }

  public locationChanged(event) {
    if (!event || !event.target) {
      return;
    }
    this.filterSearch({
      locationId: event.target.value,
    });
  }

  public typeChanged(event) {
    const value = event.target.value;
    this.filterSearch({
      type: value,
    });
  }

  public search(event) {
    this.filterSearch({
      search: event,
    });
  }

  private filterSearch(data) {
    const result = {};
    for (const i of this.filterFields) {
      result[i] = null != data[i] ? data[i] : this.pageParams[i];
    }

    this.router.navigate([], { queryParams: result });
  }

  public onSort(data) {
    this.queryParams({
      sortBy: data.column,
      sortOrder: data.order,
    });
  }

  public onPageChanged(pageNumber) {
    this.queryParams({
      pageNumber: pageNumber,
    });
  }

  private queryParams(params) {
    const result = this.routeHelperService.queryParams(params, this.pageParams);
    this.router.navigate([], { queryParams: result });
  }

  private loadItems(data) {
    this.loading = true;
    this.clientService.getClients(data)
      .subscribe(result => {
        const jsonData = result.json();
        this.items = jsonData.items;
        this.totalItems = jsonData.totalItems;
        this.loading = false;
      });
  }

  public async block(item: any) {
    this.itemBlocking[item.id] = true;
    const success = await this.translate.getTranslatedText('block-success', item);
    const error = await this.translate.getTranslatedText('block-error', item);

    this.clientService.block(item.id)
      .subscribe(resp => {
        item.isBlocked = true;
        delete this.itemBlocking[item.id];
        this.alertService.success(success);
      }, e => {
        this.alertService.error(error);
      });
  }

  public async unblock(item: any) {
    this.itemBlocking[item.id] = true;
    const success = await this.translate.getTranslatedText('unblock-success', item);
    const error = await this.translate.getTranslatedText('unblock-error', item);

    this.clientService.unblock(item.id)
      .subscribe(resp => {
        item.isBlocked = false;
        delete this.itemBlocking[item.id];
        this.alertService.success(success);
      }, e => {
        this.alertService.error(error);
      });
  }

  public async resetPassword(item: any) {
    this.itemLoading[item.id] = true;
    const success = await this.translate.getTranslatedText('password-reset--success', item);
    const error = await this.translate.getTranslatedText('password-reset--error', item);

    this.clientService.resetPassword(item.email)
      .subscribe(resp => {
        delete this.itemLoading[item.id];
        this.alertService.success(success);
      }, e => {
        this.alertService.error(error);
      });
  }

  get apiUrl() {
    return config.api_url;
  }

  get queryString() {
    return this.routeHelperService.buildQueryString({
      'x-auth-token': this.authDataStorageService.getSessionTokenFromStorage(),
      ...this.pageParams
    }, ['pageSize', 'pageNumber']);
  }

  public get totalItemsO() {
    return { totalItems: this.totalItems };
  }

}
