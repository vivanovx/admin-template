import { Injectable } from '@angular/core';
import { CHttp } from '../../core/services/chttp.service';
import { config } from '../../app.config';

@Injectable()
export class ClientService {

  constructor(private http: CHttp) { }

  public getClients(params?: any) {
    return this.http.get(config.api_url + 'users', { params });
  }

  public getById(id: string) {
    return this.http.get(config.api_url + `users/${id}`);
  }

  public resetPassword(email: string) {
    return this.http.post(config.api_url + `users/reset-password`, { email });
  }

  public block(id: string) {
    return this.http.post(config.api_url + `users/${id}/block`);
  }

  public unblock(id: string) {
    return this.http.post(config.api_url + `users/${id}/unblock`);
  }

}
