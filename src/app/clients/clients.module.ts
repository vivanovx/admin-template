import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClientListingComponent } from './client-listing/client-listing.component';
import { ClientDetailsComponent } from './client-details/client-details.component';
import { ClientService } from './services/client.service';
import { CoreModule } from '../core/core.module';
import { FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    CoreModule,
    FormsModule,
    TranslateModule.forRoot()
  ],
  exports: [
    ClientListingComponent,
    ClientDetailsComponent
  ],
  providers: [ClientService],
  declarations: [ClientListingComponent, ClientDetailsComponent]
})
export class ClientsModule { }
