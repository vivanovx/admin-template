import { Component, OnInit } from '@angular/core';
import { Router, Params, ActivatedRoute } from '@angular/router';
import { BaseEditComponent } from '../../core/common/base-component';
import { ClientService } from '../services/client.service';
import { RouteHelperService } from '../../core/services/route-helper.service';
import { CTranslateService } from '../../core/services/translate.service';
import { FileViewData } from '../../core/file-upload/file-view-data';

@Component({
  selector: 'ch-client-details',
  templateUrl: './client-details.component.html',
  styleUrls: ['./client-details.component.css']
})
export class ClientDetailsComponent extends BaseEditComponent implements OnInit {

  public clientData: any = {};
  public imageData: FileViewData;
  public cvData: FileViewData;

  constructor(
    private router: Router,
    private clientService: ClientService,
    private activatedRoute: ActivatedRoute,
    private routeHelperService: RouteHelperService,
    private translate: CTranslateService
  ) {
    super();
  }

  ngOnInit() {
    this.activatedRoute.queryParamMap
      .map((params: Params) => params.params)
      .subscribe(params => {
        this.getClient(params.id);
      });

    (async () => {
      this.texts.clients = await this.translate.getTranslatedText('clients:sidebar');
    })();
  }

  private getClient(id: string) {
    this.loading = true;
    this.clientService.getById(id)
      .subscribe(resp => {
        const jsonData = resp.json();
        this.clientData = resp.json();
        this.imageData = { resourceId: '588f296a-aae1-4ad9-bc38-30593300882c' };
        this.cvData = null;
        this.loading = false;
        this.loaded = true;
      });
  }

  public cancel() {
    this.router.navigate(['/admin/clients']);
  }

}
