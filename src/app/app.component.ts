import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { CTranslateService } from './core/services/translate.service';

@Component({
  selector: 'ch-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(private translate: CTranslateService) {
    translate.setLanguage();
  }

}
