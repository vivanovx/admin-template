import { Component, OnInit } from '@angular/core';
import { Params, Router, ActivatedRoute } from '@angular/router';
import { AdminUsersService } from '../services/admin-users.service';
import { FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';
import { RouteHelperService } from '../../core/services/route-helper.service';
import { LocationService } from '../../locations/services/location.service';
import { BaseEditComponent } from '../../core/common/base-component';
import { AuthDataStorageService } from '../../auth/services/auth-data-storage.service';
import swal from 'sweetalert2';
import { AlertService } from '../../core/services/alert.service';
import { CTranslateService } from '../../core/services/translate.service';

@Component({
  selector: 'ch-edit-admin-user',
  templateUrl: './edit-admin-user.component.html',
  styleUrls: ['./edit-admin-user.component.css']
})
export class EditAdminUserComponent extends BaseEditComponent implements OnInit {

  private userId: string;
  private params: any;
  public initialPageBreadcrumbName: string;
  public editMode: boolean;
  public userData: any = {};
  public userRoles = [
    {
      value: 'ADMIN',
      name: 'Super Admin'
    },
    {
      value: 'CUSTOMER_SUPPORT',
      name: 'Customer Support'
    }
  ];
  public userName: string;
  public errors: any = {};

  constructor(
    private alertService: AlertService,
    private adminUsersService: AdminUsersService,
    private activatedRoute: ActivatedRoute,
    protected router: Router,
    private routeHelperService: RouteHelperService,
    private locationService: LocationService,
    private translate: CTranslateService,
    protected authDataStorageService: AuthDataStorageService,
  ) { super(); }

  ngOnInit() {
    this.activatedRoute.queryParamMap
      .map((params: Params) => params.params)
      .subscribe((params) => {
        this.params = params;
        this.editMode = this.isEditPage();
        this.userId = this.getCurrentUserId();

        if (this.editMode) {
          this.fetchData(this.userId);
        }
      });

    (async () => {
      this.initialPageBreadcrumbName = await this.translate.getTranslatedText('admin-users:sidebar');
      this.texts.next = await this.translate.getTranslatedText('next:edit');
      this.texts.save = await this.translate.getTranslatedText('save:edit');
      this.texts.addNew = await this.translate.getTranslatedText('add-new:edit');
      this.texts.currentPassword = await this.translate.getTranslatedText('current-password:edit');
      this.texts.newPassword = await this.translate.getTranslatedText('new-password:edit');
      this.texts.confirmPassword = await this.translate.getTranslatedText('confirm-password:edit');
      this.texts.enterSomething = await this.translate.getTranslatedText('enter-something:edit');
      this.texts.invalidData = await this.translate.getTranslatedText('invalid-data:edit');
      this.texts.deleteConfirmation = await this.translate.getTranslatedText('delete-confirmation:edit');
      this.texts.deleteUsedError = await this.translate.getTranslatedText('delete-used-error:edit');
      this.texts.deleteError = await this.translate.getTranslatedText('delete-error:edit');

      this.texts.ops = await this.translate.getTranslatedText('ops:edit');
      this.texts.passwordConfirmationError = await this.translate.getTranslatedText('password-confirmation-error:edit');
      this.texts.passwordConfirmationMissmatch = await this.translate.getTranslatedText('password-confirmation-missmatch:edit');
      this.texts.congratulations = await this.translate.getTranslatedText('congratulations:edit');
      this.texts.passwordChangeSuccess = await this.translate.getTranslatedText('password-change-success:edit');
    })();
  }

  public changePassword() {
    this.openChangePasswordPopup()
      .then(result => {
        swal.resetDefaults();
        if (!result.value) {
          return;
        }
        const currentPassword = result.value[0];
        const newPassword = result.value[1];
        const confirmedPassword = result.value[2];
        if (newPassword !== confirmedPassword) {
          swal(this.texts.ops, this.texts.passwordConfirmationError, 'error')
            .then(() => {
              this.changePassword();
            });

          return;
        }
        if (currentPassword === newPassword) {
          swal(this.texts.ops, this.texts.passwordConfirmationMissmatch, 'error')
            .then(() => {
              this.changePassword();
            });

          return;
        }

        this.adminUsersService.changePassword(currentPassword, newPassword)
          .subscribe(resp => {
            swal(this.texts.congratulations, this.texts.passwordChangeSuccess, 'success');
          });
      }, () => {
        swal.resetDefaults();
      });
  }

  private openChangePasswordPopup() {
    swal['setDefaults']({
      input: 'password'
    });

    swal['setDefaults']({
      input: 'password',
      confirmButtonText: `${this.texts.next} &rarr;`,
      // showCancelButton: true,
      type: 'question',
      showLoaderOnConfirm: true,
      progressSteps: ['1', '2', '3'],
      inputValidator: (value) => {
        return new Promise((resolve, reject) => {
          if (value) {
            resolve(null);
          } else {
            resolve(this.texts.enterSomething);
          }
        });
      }
    });

    const steps = [
      this.texts.currentPassword,
      this.texts.newPassword,
      this.texts.confirmPassword
    ];

    return swal.queue(steps);
  }

  protected getCurrentUserId() {
    return this.params.id;
  }

  protected isEditPage() {
    return this.routeHelperService.isCurrentPageEdit();
  }

  private fetchData(id: string) {
    this.loading = true;
    this.adminUsersService.getUser(id)
      .subscribe(data => {
        this.userData = data.json();
        this.userName = this.userData.name;
        this.loading = false;
        this.loaded = true;
      }, error => {
        this.loading = false;
      });
  }

  public edit(form) {
    this.setFormLoading(true);
    if (!form.valid) {
      for (const i of Object.keys(form.controls)) {
        form.controls[i].markAsTouched();
      }

      this.alertService.error(this.texts.invalidData);
      this.setFormLoading(false);
      return;
    }

    this.loading = true;
    const methodName = this.editMode ? 'updateUser' : 'addUser';
    this.adminUsersService[methodName](this.userData)
      .subscribe(
      result => {
        this.loading = false;
        this.setFormLoading(false);

        this.cancel();
      },
      error => {
        this.loading = false;
        this.setFormLoading(false);

        for (const item of error.errors instanceof Array ? error.errors : []) {
          if (!form.controls[item.field]) {
            continue;
          }
          form.controls[item.field].setErrors({ [item.field]: item.message });
          this.errors[item.field] = item.message;
        }
      });
  }

  public cancel() {
    this.router.navigateByUrl('/admin/admin-users');
  }

  public resetPassword(isDirtyUsername) {
    if (isDirtyUsername) {
      return;
    }

    this.changePassword();
  }

  public block() {
    const methodName = this.userData.isBlocked ? 'unblockUser' : 'blockUser';
    this.adminUsersService[methodName](this.userId)
      .subscribe(
      resp => {
        this.userData.isBlocked = !this.userData.isBlocked;
      },
      error => { }
      );
  }

  public delete() {
    if (!confirm(this.texts.deleteConfirmation)) {
      return;
    }

    this.adminUsersService.deleteUser(this.userId)
      .subscribe(
      resp => {
        this.cancel();
      },
      error => {
        if (409 === error.status) {
          this.alertService.error(this.texts.deleteUsedError);
        } else {
          this.alertService.error(this.texts.deleteError);
        }
      }
      );
  }

  get isLoggedUser() {
    return this.userId === this.authDataStorageService.getLoggedUserId();
  }

}
