import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AdminUsersService } from '../services/admin-users.service';
import { RouteHelperService } from '../../core/services/route-helper.service';
import { LocationService } from '../../locations/services/location.service';
import { EditAdminUserComponent } from './edit-admin-user.component';
import { AuthService } from '../../auth/services/auth.service';
import { AuthDataStorageService } from '../../auth/services/auth-data-storage.service';
import { AlertService } from '../../core/services/alert.service';
import { CTranslateService } from '../../core/services/translate.service';

@Component({
  selector: 'ch-edit-admin-user',
  templateUrl: './edit-admin-user.component.html',
  styleUrls: ['./edit-admin-user.component.css']
})
export class ProfileEditComponent extends EditAdminUserComponent {

  constructor(
    alertService: AlertService,
    adminUsersService: AdminUsersService,
    activatedRoute: ActivatedRoute,
    router: Router,
    routeHelperService: RouteHelperService,
    locationService: LocationService,
    authDataStorageService: AuthDataStorageService,
    translate: CTranslateService
  ) {
    super(
      alertService,
      adminUsersService,
      activatedRoute,
      router,
      routeHelperService,
      locationService,
      translate,
      authDataStorageService
    );

    (async () => {
      this.initialPageBreadcrumbName = await translate.getTranslatedText('profile-edit:edit');
    })();
  }

  protected getCurrentUserId() {
    return this.authDataStorageService.getLoggedUserId();
  }

  protected isEditPage() {
    return true;
  }

  public cancel() {
    this.router.navigateByUrl('/');
  }

}
