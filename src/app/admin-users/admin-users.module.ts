import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { CoreModule } from '../core/core.module';
import { LocationsModule } from '../locations/locations.module';
import { AdminUsersListingComponent } from './admin-users-listing/admin-users-listing.component';
import { AdminUsersService } from './services/admin-users.service';
import { EditAdminUserComponent } from './edit-admin-user/edit-admin-user.component';
import { ProfileEditComponent } from './edit-admin-user/profile-edit.component';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    CoreModule,
    FormsModule,
    LocationsModule,
    TranslateModule.forRoot()
  ],
  exports: [
    AdminUsersListingComponent,
    EditAdminUserComponent,
    ProfileEditComponent
  ],
  providers: [
    AdminUsersService
  ],
  declarations: [AdminUsersListingComponent, EditAdminUserComponent, ProfileEditComponent]
})
export class AdminUsersModule { }
