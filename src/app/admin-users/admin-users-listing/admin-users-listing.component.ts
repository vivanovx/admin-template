import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { AdminUsersService } from '../services/admin-users.service';
import { SortableComponent } from '../../core/sortable/sortable.component';
import { RouteHelperService } from '../../core/services/route-helper.service';

@Component({
  selector: 'ch-admin-users-listing',
  templateUrl: './admin-users-listing.component.html',
  styleUrls: ['./admin-users-listing.component.css']
})
export class AdminUsersListingComponent implements OnInit {

  public items = [];
  public totalItems;
  public pageParams: any = {};
  private filterFields: string[] = ['search'];

  public loading: boolean;

  constructor(
    private adminUsersService: AdminUsersService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private routeHelperService: RouteHelperService
  ) { }

  ngOnInit() {
    this.activatedRoute.queryParamMap
      .map((params: Params) => params.params)
      .subscribe((params) => {
        this.pageParams = this.routeHelperService.getValidatedParams(params);
        this.loadItems(this.pageParams);
      });
  }

  public editUser(id: string) {
    this.router.navigate(['/admin/admin-users/edit'], {
      queryParams: {
        id: id
      }
    });

    return false;
  }

  public addNew() {
    this.router.navigate(['/admin/admin-users/add']);

    return false;
  }

  public search(event) {
    this.filterSearch({
      search: event,
    });
  }

  private filterSearch(data) {
    const result = {};
    for (const i of this.filterFields) {
      result[i] = null != data[i] ? data[i] : this.pageParams[i];
    }

    this.router.navigate([], { queryParams: result });
  }

  public onSort(data) {
    this.queryParams({
      sortBy: data.column,
      sortOrder: data.order,
    });
  }

  public onPageChanged(pageNumber) {
    this.queryParams({
      pageNumber: pageNumber,
    });
  }

  private queryParams(params) {
    const result = this.routeHelperService.queryParams(params, this.pageParams);
    this.router.navigate([], { queryParams: result });
  }

  private loadItems(data) {
    this.loading = true;
    this.adminUsersService.getAdminUsers(data)
      .subscribe(result => {
        const jsonData = result.json();
        this.items = jsonData.items;
        this.totalItems = jsonData.totalItems;
        this.loading = false;
      });
  }

  public get totalItemsO() {
    return {
      totalItems: this.totalItems
    };
  }

}
