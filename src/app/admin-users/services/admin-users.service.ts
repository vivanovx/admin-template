import { Injectable } from '@angular/core';
import { CHttp } from '../../core/services/chttp.service';
import { config } from '../../app.config';

@Injectable()
export class AdminUsersService {

  constructor(private http: CHttp) { }

  public getAdminUsers(data?: any) {
    return this.http.get(this.getBaseUrl(), { params: data });
  }

  public getUser(id: string) {
    return this.http.get(`${this.getBaseUrl()}${id}`);
  }

  public addUser(data) {
    return this.http.post(this.getBaseUrl(), data);
  }

  public updateUser(data) {
    return this.http.put(this.getBaseUrl(), data);
  }

  public changePassword(currentPassword: string, newPassword: string) {
    return this.http.post(`${this.getBaseUrl()}/change-password`, {
      oldPassword: currentPassword,
      newPassword: newPassword
    });
  }

  public forgottenPassword(username: string) {
    return this.http.post(`${this.getBaseUrl()}/forgotten-password`, { 'username': username });
  }

  public deleteUser(id: string) {
    return this.http.delete(`${this.getBaseUrl()}${id}`);
  }

  public blockUser(id: string) {
    return this.http.post(`${this.getBaseUrl()}${id}/block`);
  }

  public unblockUser(id: string) {
    return this.http.post(`${this.getBaseUrl()}${id}/unblock`);
  }

  private getBaseUrl() {
    return `${config.api_url}admin-users/`;
  }

}
