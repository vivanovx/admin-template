import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { LoginComponent } from './login/login.component';
import { AuthService } from './services/auth.service';
import { AuthDataStorageService } from './services/auth-data-storage.service';
import { AuthGuard } from './guard/auth.guard';
import { SuperUserRequiredGuard } from './guard/super-user-required.guard';
import { ClientGuard } from './guard/client.guard';
import { LoggedInGuard } from './guard/logged.guard';
import { ForgottenPasswordComponent } from './forgotten-password/forgotten-password.component';
import { CoreModule } from '../core/core.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    CoreModule,
    BrowserModule,
    FormsModule,
    RouterModule,
    TranslateModule.forRoot()
  ],
  exports: [
    LoginComponent
  ],
  providers: [
    AuthService,
    AuthDataStorageService,
    AuthGuard,
    SuperUserRequiredGuard,
    ClientGuard,
    LoggedInGuard
  ],
  declarations: [LoginComponent, ForgottenPasswordComponent]
})
export class AuthModule { }
