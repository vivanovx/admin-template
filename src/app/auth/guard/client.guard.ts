import { Injectable } from '@angular/core';
import {
  Router,
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from '@angular/router';
import { AuthService } from '../services/auth.service';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import { AuthDataStorageService } from '../services/auth-data-storage.service';

@Injectable()
export class ClientGuard implements CanActivate {
  private subscription: Subscription;

  constructor(
    private router: Router,
    private authDataStorageService: AuthDataStorageService,
    private authService: AuthService
  ) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.authService.verifyUserIsLoggedIn().map(e => {
      if (e) {
        if (this.authDataStorageService.isSuperAdmin() || this.authDataStorageService.isCustomerSupport()) {
          return true;
        } else if (this.authDataStorageService.isDoctorMonitoring()) {
          this.router.navigateByUrl('/admin/service-providers');
        }

        return false;
      }
    })
      .catch(() => {
        this.router.navigate(['/login']);

        return Observable.of(false);
      });
  }

}
