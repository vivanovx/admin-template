import { Injectable } from '@angular/core';
import {
  Router,
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from '@angular/router';
import { AuthService } from '../services/auth.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

@Injectable()
export class LoggedInGuard implements CanActivate {

  constructor(
    private router: Router,
    private authService: AuthService
  ) { }

  canActivate(route: ActivatedRouteSnapshot) {
    return this.authService.verifyUserIsLoggedIn()
      .map(e => {
        if (e) {
          // user is logged
          this.router.navigate(['/admin/dashboard']);

          return false;
        }
      })
      .catch((error) => {
        // user is not logged
        return Observable.of(true);
      });
  }
}
