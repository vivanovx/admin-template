import { Injectable } from '@angular/core';

@Injectable()
export class AuthDataStorageService {
  public static readonly STORAGE_ITEM_NAME = 'token';

  constructor() { }

  public setUserDataInStorageFromJson(data) {
    this.setUserDataInStorageFromString(JSON.stringify(data));
  }

  public setUserDataInStorageFromString(data) {
    localStorage.setItem(AuthDataStorageService.STORAGE_ITEM_NAME, data);
  }

  public removeTokenFromStorage() {
    localStorage.removeItem(AuthDataStorageService.STORAGE_ITEM_NAME);
  }

  public getLoggedUserId() {
    const authData = this.getStorageItemDataAsJson();

    return (authData && authData.user) ? authData.user.id : null;
  }

  public isUserRestricted() {
    const user = this.getAuthUserData();

    return false;
  }

  public isSuperAdmin() {
    const user = this.getAuthUserData();

    return 'ADMIN' === user.role;
  }

  public isCustomerSupport() {
    const user = this.getAuthUserData();

    return 'CUSTOMER_SUPPORT' === user.role;
  }

  public isDoctorMonitoring() {
    const user = this.getAuthUserData();

    return 'DOCTOR_MONITORING' === user.role;
  }

  private getAuthUserData() {
    const authData = this.getStorageItemDataAsJson();
    const user = (authData && authData.user) || {};

    return user;
  }

  public getSessionTokenFromStorage() {
    const authData = this.getStorageItemDataAsJson();
    const token = authData ? authData.sessionId : null;

    return token;
  }

  public getLoggedUserData() {
    return this.getStorageItemDataAsJson();
  }

  private getStorageItemDataAsJson() {
    let data = null;
    try {
      data = JSON.parse(this.getRawStorageItemData());
    } catch (e) { }

    return data;
  }

  private getRawStorageItemData() {
    return localStorage.getItem(AuthDataStorageService.STORAGE_ITEM_NAME);
  }

}
