import { Injectable } from '@angular/core';
import { LoginData } from '../user-login';
import { ResetPassword } from '../reset-password';
import { config } from '../../app.config';
import { Http } from '@angular/http';
import { RequestOptions, RequestOptionsArgs, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { AuthDataStorageService } from './auth-data-storage.service';

@Injectable()
export class AuthService {

  constructor(
    private http: Http,
    private authDataService: AuthDataStorageService
  ) { }

  public isLoggedIn(): boolean {
    const token: string = this.authDataService.getSessionTokenFromStorage();

    return null !== token && 'string' === typeof token && '' !== token.trim();
  }

  public verifyUserIsLoggedIn() {
    return this.http.get(config.api_url + 'sessions', this.addAuthTokenIfHas())
      .catch(error => {
        if ('string' === typeof error) {
          try {
            error = JSON.parse(error);
          } catch (ex) {
            error = {};
          }
        }
        if (!error.ok && 401 === error.status) {
          // user already logged out
          this.authDataService.removeTokenFromStorage();
        }

        return Observable.throw(error);
      });
  }

  public login(data: LoginData) {
    return new Promise((resolve, reject) => {
      this.http.post(config.api_url + 'sessions', data)
        .subscribe(resp => {
          this.authDataService.setUserDataInStorageFromJson(resp.json());

          resolve(resp);
        }, error => {
          reject(error);
        });
    });
  }

  public forgottenPassword(data) {
    return this.http.post(config.api_url + 'admin-users/forgotten-password', data, this.addContentType());
  }

  public logout() {
    return new Promise((resolve, reject) => {
      this.http.delete(config.api_url + 'sessions', this.addAuthTokenIfHas())
        .subscribe(resp => {
          this.authDataService.removeTokenFromStorage();
          resolve(true);
        }, error => {
          if (!error.ok && 401 === error.status) {
            // user already logged out
            this.authDataService.removeTokenFromStorage();
            resolve(true);

            return;
          }

          reject(error);
        });
    });
  }

  private addAuthTokenIfHas(options?: RequestOptionsArgs): RequestOptionsArgs {
    options = options || new RequestOptions();
    options.headers = options.headers || new Headers();

    const token = this.authDataService.getSessionTokenFromStorage();
    if (token) {
      options.headers.append('X-Auth-Token', token);
    }

    return options;
  }

  private addContentType(options?: RequestOptionsArgs): RequestOptionsArgs {
    options = options || new RequestOptions();
    options.headers = options.headers || new Headers();
    options.headers.append('Content-Type', 'application/json');

    return options;
  }

  public verifyLoggedUser() {
    return this.http.get(config.api_url + 'sessions');
  }

}
