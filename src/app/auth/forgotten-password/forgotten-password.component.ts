import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { ResetPassword } from '../reset-password';
import { AdminUsersService } from '../../admin-users/services/admin-users.service';

@Component({
  selector: 'ch-forgotten-password',
  templateUrl: './forgotten-password.component.html',
  styleUrls: ['./forgotten-password.component.css']
})
export class ForgottenPasswordComponent implements OnInit {

  public loading: boolean;
  public formData: ResetPassword = { username: '' };

  constructor(
    private authService: AuthService,
    private adminUserService: AdminUsersService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  public resetPassword(form) {
    if (!form.valid) {
      for (const i of Object.keys(form.controls)) {
        form.controls[i].markAsTouched();
      }

      return;
    }

    this.loading = true;
    this.adminUserService.forgottenPassword(this.formData.username)
      .subscribe(result => {
        this.login();

        this.loading = false;
      }, error => {

        this.loading = false;
      });
  }

  public login() {
    this.router.navigateByUrl('login');

    return false;
  }

}
