import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { LoginData } from '../user-login';
import { AuthDataStorageService } from '../services/auth-data-storage.service';

@Component({
  selector: 'ch-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public loading: boolean;
  public loginData: LoginData = {
    username: '',
    password: ''
  };

  constructor(
    private authDataStorageService: AuthDataStorageService,
    private authService: AuthService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  public login(form) {
    if (!form.valid) {
      for (const i of Object.keys(form.controls)) {
        form.controls[i].markAsTouched();
      }

      return;
    }

    this.loading = true;
    this.authService.login(this.loginData)
      .then(() => {
        this.loading = false;
        if (this.authDataStorageService.isDoctorMonitoring()) {
          this.router.navigateByUrl('/admin/service-providers');
        } else if (this.authDataStorageService.isCustomerSupport()) {
          this.router.navigateByUrl('/admin/clients');
        } else {
          this.router.navigateByUrl('/admin/dashboard');
        }
      })
      .catch(() => {
        this.loading = false;
      });
  }

  public forgottenPassword() {
    this.router.navigateByUrl('forgotten-password');

    return false;
  }

}
