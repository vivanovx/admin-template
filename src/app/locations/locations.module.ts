import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { CoreModule } from '../core/core.module';
import { LocationListingComponent } from './location-listing/location-listing.component';
import { LocationEditComponent } from './location-edit/location-edit.component';
import { LocationService } from './services/location.service';
import { AgmCoreModule } from '@agm/core';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    CoreModule,
    FormsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAhwtYnaefUYn16x7B1dFjHy4WMv6KALTY'
    }),
    TranslateModule.forRoot()
  ],
  exports: [
    LocationListingComponent,
    LocationEditComponent
  ],
  providers: [LocationService],
  declarations: [LocationListingComponent, LocationEditComponent]
})
export class LocationsModule { }
