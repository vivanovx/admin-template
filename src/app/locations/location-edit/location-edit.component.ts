import { Component, OnInit } from '@angular/core';
import { Params, Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';
import { RouteHelperService } from '../../core/services/route-helper.service';
import { LocationService } from '../services/location.service';
import * as moment from 'moment';
import { localeData } from 'moment';
import { BaseEditComponent } from '../../core/common/base-component';
import { FormControl } from '@angular/forms/src/model';
import { AlertService } from '../../core/services/alert.service';
import { CTranslateService } from '../../core/services/translate.service';

@Component({
  selector: 'ch-location-edit',
  templateUrl: './location-edit.component.html',
  styleUrls: ['./location-edit.component.css']
})
export class LocationEditComponent extends BaseEditComponent implements OnInit {

  private locationId: string;
  public locationData: any = {};
  public locationName: string;
  private type: string;
  private parentId: string;

  public errors: any = {};
  public chosenLocation = false;

  public breadcrumb: any[] = [];

  constructor(
    private alertService: AlertService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private locationService: LocationService,
    private routeHelperService: RouteHelperService,
    private translate: CTranslateService
  ) { super(); }

  ngOnInit() {
    this.activatedRoute.queryParamMap
      .map((params: Params) => params.params)
      .subscribe((params) => {
        this.editMode = this.routeHelperService.isCurrentPageEdit();
        this.locationId = params.id;
        this.parentId = params.parentId;

        if (this.editMode) {
          this.fetchData(this.locationId);
          this.setLocationBreadcrumb(this.locationId);
        } else {
          this.type = 'string' === typeof params.type ? params.type.trim().toLowerCase() : '';
          this.setLocationBreadcrumb(this.parentId, this.texts.addNew);
        }
      });

    this.setTexts();
  }

  private async setTexts() {
      this.texts.save = await this.translate.getTranslatedText('save:edit');
      this.texts.invalidData = await this.translate.getTranslatedText('invalid-data:edit');
      this.texts.addNew = await this.translate.getTranslatedText('add-new:edit');
      this.texts.deleteConfirmation = await this.translate.getTranslatedText('delete-confirmation:edit');
      this.texts.deleteUsedError = await this.translate.getTranslatedText('delete-used-error:edit');
      this.texts.deleteError = await this.translate.getTranslatedText('delete-error:edit');
  }

  private async setLocationBreadcrumb(id: string, last?: string) {
    this.breadcrumb = await this.locationService.getLocationBreadcrumb(id, this.editMode, last);
  }

  public breadcrumbGoTo(data) {
    const target = data.data;
    if ('top' === target) {
      this.router.navigate(['/admin/locations']);
    } else {
      this.goToSubroute(target);
    }
  }

  public goToSubroute(id: string) {
    this.router.navigate(['/admin/locations'], {
      queryParams: { parentId: id }
    });

    return false;
  }

  private fetchData(id: string) {
    this.loading = true;
    this.locationService.getById(id)
      .subscribe(data => {
        this.locationData = data.json();
        this.locationName = this.locationData['name'];
        this.type = this.locationData.isCountry ? 'country' : (
          this.locationData.isCity ? 'city' : ''
        );

        if (this.locationData.isCountry) {
          this.locationData.countryName = this.locationData.nameEn;
        } else {
          this.locationData.cityName = this.locationData.nameEn;
        }

        if (this.locationData.latitude && this.locationData.longitude) {
          this.chosenLocation = true;
        }
      }, () => { }, () => { this.loading = false; this.loaded = true; });
  }

  public edit(form) {
    this.setFormLoading(true);
    if (!form.valid) {
      for (const control of Object.keys(form.controls)) {
        form.controls[control].markAsTouched();
      }

      this.setFormLoading(false);
      this.alertService.error(this.texts.invalidData);
      return;
    }

    const reqData: any = {};
    for (const prop of Object.keys(this.locationData)) {
      reqData[prop] = this.locationData[prop];
    }

    const period = parseInt(this.locationData.trialPeriod);
    if (!isNaN(period)) {
      reqData.trialPeriod = moment.duration({ months: period }).toISOString();
    }

    if (this.parentId) {
      reqData.parentLocationId = this.parentId;
    }

    delete reqData.cityName;
    delete reqData.countryName;

    const methodName = this.editMode ? 'update' : 'add';

    if ('boolean' !== typeof reqData.isCity) {
      reqData.isCity = false;
    }
    if ('boolean' !== typeof reqData.isCountry) {
      reqData.isCountry = false;
    }

    this.loading = true;
    this.locationService[methodName](reqData)
      .subscribe(resp => {
        this.loading = false;
        this.cancel();
      }, err => {
        this.loading = false;
        if (!(err.errors instanceof Array)) {
          return;
        }
        for (const item of err.errors) {
          this.errors[item.field] = item.message;
        }
      }, () => {
        this.setFormLoading(false);
      });
  }

  public cancel() {
    const targetLocationId = this.editMode ? this.locationData.parentLocationId : this.parentId;
    const url = '/admin/locations' + (targetLocationId ? ('?parentId=' + targetLocationId) : '');
    this.router.navigateByUrl(url);
  }

  public markAsCity(checked) {
    this.locationData.isNeighbourhood = false;
    this.locationData.isCity = checked;
    this.locationData.isCountry = false;
  }

  public markAsCountry(checked) {
    this.locationData.isNeighbourhood = false;
    this.locationData.isCity = false;
    this.locationData.isCountry = checked;
  }

  public markAsNeighbourhood(checked) {
    this.locationData.isNeighbourhood = checked;
    this.locationData.isCity = false;
    this.locationData.isCountry = false;
  }

  public onLocationChange(event: any) {
    this.locationData.latitude = event.coords.lat;
    this.locationData.longitude = event.coords.lng;

    this.chosenLocation = true;
  }

  public delete() {
    if (!confirm(this.texts.deleteConfirmation)) {
      return;
    }

    this.locationService.delete(this.locationId)
      .subscribe(resp => { this.cancel(); }, error => {
        if (409 === error.status) {
          this.alertService.error(this.texts.deleteUsedError);
        } else {
          this.alertService.error(this.texts.deleteError);
        }
      });
  }

}
