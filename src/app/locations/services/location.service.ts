import { Injectable } from '@angular/core';
import { CHttp } from '../../core/services/chttp.service';
import { config } from '../../app.config';
import { locale } from 'moment';
import { CTranslateService } from '../../core/services/translate.service';

@Injectable()
export class LocationService {

  constructor(
    private http: CHttp,
    private translate: CTranslateService
  ) { }

  public getLocationParentTree(id: string) {
    return new Promise((resolve, reject) => {
      const self = this;
      const data = [];
      self.getById(id)
        .subscribe(handle);

      function handle(resp) {
        if (200 === resp.status) {
          const respData = resp.json();
          data.unshift({
            name: respData.nameBg,
            id: respData.id,
            parentId: respData.parentLocationId
          });
          if (!respData.parentLocationId) {
            resolve(data);
            return;
          }

          self.getById(respData.parentLocationId)
            .subscribe(handle);
        } else {
          resolve(data);
        }
      }
    });
  }

  public async getHeighbourhoodData() {
    const locations: any = await this.getLocationData();
    const result = [];
    for (const item of locations) {
      if (item.isNeighbourhood) {
        result.push(item);
      }
    }

    return result;
  }

  public getLocationData() {
    return new Promise((resolve, reject) => {
      this.getAll()
        .subscribe(
        resp => {
          const jsonResult = resp.json();
          const locations: any[] = [];
          for (const item of jsonResult) {
            if (isEmptyString(item.nameBg)) {
              continue;
            }

            const name: string[] = [];
            let ti = item;
            while (ti) {
              name.unshift(ti.nameBg);
              ti = getParentLocation(ti.parentLocationId);
            }
            locations.push({
              value: name.join(', '),
              id: item.id,
              isCity: item.isCity,
              isCountry: item.isCountry,
              isNeighbourhood: item.isNeighbourhood
            });
          }

          locations.sort((a, b) => a.value.localeCompare(b.value));
          resolve(locations);

          function getParentLocation(id: string) {
            for (const item of jsonResult) {
              if (isEmptyString(item.nameBg)) {
                continue;
              }

              if (id === item.id) {
                return item;
              }
            }

            return null;
          }

          function isEmptyString(str) {
            return 'string' !== typeof str || '' === str.trim();
          }
        }
        );
    });
  }

  public async getLocationBreadcrumb(id: string, editMode: boolean = true, last?: string) {
    const locationsText = await this.translate.getTranslatedText('locations:sidebar');
    const addNewText = await this.translate.getTranslatedText('add-new:edit');

    let breadcrumb = [];
    if (!id) {
      breadcrumb = [locationsText, editMode ? '' : addNewText];

      return Promise.resolve(breadcrumb);
    }

    const resp: any = await this.getLocationParentTree(id);
    if (last) {
      resp.push(last);
    }

    const a = [locationsText, ...resp];
    const result = [];

    for (let i = 0; i < a.length; i++) {
      const item = a[i];
      if ('object' === typeof (item)) {
        result.push({
          name: item.name,
          data: item.id
        });
      } else {
        result.push(0 === i ? {
          name: item,
          data: 'top'
        } : item);
      }
    }
    breadcrumb = result;

    return Promise.resolve(breadcrumb);
  }

  public getAll() {
    return this.http.get(config.api_url + 'locations/all');
  }

  public getLocations(data?: any) {
    return this.http.get(config.api_url + 'locations', { params: data });
  }

  public getById(id: string) {
    return this.http.get(config.api_url + 'locations/' + id);
  }

  public getLocationCities() {
    return this.http.get(config.api_url + 'locations/cities');
  }

  public add(data: any) {
    return this.http.post(config.api_url + 'locations', data);
  }

  public update(data: any) {
    return this.http.put(config.api_url + 'locations', data);
  }

  public delete(id: string) {
    return this.http.delete(config.api_url + 'locations/' + id);
  }

  public getSubLocations(id: string, data?: any) {
    return this.http.get(config.api_url + 'locations/' + id + '/locations', { params: data });
  }

}
