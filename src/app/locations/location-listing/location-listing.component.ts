import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { LocationService } from '../services/location.service';
import { SortableComponent } from '../../core/sortable/sortable.component';
import { RouteHelperService } from '../../core/services/route-helper.service';
import { CTranslateService } from '../../core/services/translate.service';

@Component({
  selector: 'ch-location-listing',
  templateUrl: './location-listing.component.html',
  styleUrls: ['./location-listing.component.css']
})
export class LocationListingComponent implements OnInit {

  public items = [];
  public totalItems;
  public pageParams: any = {};
  public loading: boolean;
  private editMode: boolean;
  public breadcrumb: string[] = [];
  private filterFields: string[] = ['search', 'parentId'];
  public searchText: string;

  constructor(
    private locationService: LocationService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private routeHelperService: RouteHelperService,
    private translate: CTranslateService
  ) { }

  ngOnInit() {
    this.activatedRoute.queryParamMap
      .map((params: Params) => params.params)
      .subscribe((params) => {
        this.pageParams = this.routeHelperService.getValidatedParams(params);

        this.setLocationBreadcrumb(this.pageParams.parentId);

        this.loadItems(this.pageParams);
      });

    (async () => {
      this.searchText = await this.translate.getTranslatedText('search:core');
    })();
  }

  private async setLocationBreadcrumb(id: string) {
    this.breadcrumb = await this.locationService.getLocationBreadcrumb(id);
  }

  public breadcrumbGoTo(data) {
    const target = data.data;
    if ('top' === target) {
      this.router.navigate(['/admin/locations']);
    } else {
      this.goToSubroute(target);
    }
  }

  public goToSubroute(id: string) {
    this.router.navigate(['/admin/locations'], {
      queryParams: { parentId: id }
    });

    return false;
  }

  public edit(id: string) {
    this.router.navigate(['/admin/locations/edit'], {
      queryParams: { id }
    });
  }

  public addNew() {
    this.router.navigate(['/admin/locations/add'], {
      queryParams: { parentId: this.pageParams.parentId }
    });
  }

  public searchByText(event) {
    this.filterSearch({
      search: event,
    });
  }

  private filterSearch(data) {
    const result = {};
    for (const i of this.filterFields) {
      result[i] = null != data[i] ? data[i] : this.pageParams[i];
    }

    this.router.navigate([], { queryParams: result });
  }

  public search(event) {
    this.queryParams({
      query: event,
    });
  }

  public onSort(data) {
    this.queryParams({
      sortBy: data.column,
      sortOrder: data.order,
    });
  }

  public onPageChanged(pageNumber) {
    this.queryParams({
      pageNumber: pageNumber,
    });
  }

  private queryParams(params) {
    const result = this.routeHelperService.queryParams(params, this.pageParams);
    this.router.navigate([], { queryParams: result });
  }

  private loadItems(data) {
    this.loading = true;
    const handle = result => {
      const jsonData = result.json();
      this.items = jsonData.items;
      this.totalItems = jsonData.totalItems;
      this.loading = false;
    };

    if (data.parentId) {
      this.locationService.getSubLocations(data.parentId, data)
        .subscribe(handle);
    } else {
      this.locationService.getLocations(data)
        .subscribe(handle);
    }
  }

  public get totalItemsO() {
    return { totalItems: this.totalItems };
  }

}
