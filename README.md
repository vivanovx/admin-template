# Admin Template project

This project is based on Angular 5.0

# Project structure

```
src                             - base source folder
    assets                      - static assets folder
    environments                - environment config files
        environment.ts          - dev env config file
        environment.prod.ts     - prod env config file
    app                         - code for the app
        core                    - core app module containing widgets and reusable components
        auth                    - auth module containing login/logout components
        admin-users             - admin users feature component
        ...                     - other feature modules
```

## Setup option 1 (Docker)

## Dependencies
* [Docker](https://www.docker.com/)

Start: `docker rm fl-admin && docker build -t fl-admin:dev . && docker run --name fl-admin -p 6969:6969 fl-admin:dev`
Stop: `docker stop fl-admin`

## Setup option 2

## Dependencies
* [NPM](https://www.npmjs.com/)
* [Angular CLI](https://cli.angular.io/) `npm install -g @angular/cli`

Run `npm install`

## Heroku project env specific deployment

In the following <stage> is the stage name. It can be `dev`, `staging`, `prod` etc.

1) Create state config file `environments/environment.<stage>.ts` and add specific stage configuration.
2) In `.angular-cli.json` add:
```json
{
  ...
  "apps": [
    {
      ...
      "environments": {
        ...
        "<stage>": "environments/environment.<stage>.ts"
      }
    }
  ],
  ...
```

3) In `package.json` add:
```json
{
  ...
  "scripts": {
    ...
    "build:<stage>": "ng build -e=<stage> --aot=true"
  },
  ...
```

3) In `build.sh` add:

```bash
elif [ $ENV == '<stage>' ]; then
  echo 'Running <stage> build..'
  npm run build:<stage>
fi
```

4) Add heroku Config Var (environment variable) named `ENV` with value of <stage>
5) Deploy `git push heroku-<stage> HEAD:master` where `heroku-<stage>` is the heroku specific stage remote repository.


## Dependencies
* [Docker](https://www.docker.com/)

Start: `docker rm fl-admin && docker build -t fl-admin:dev . && docker run --name fl-admin -p 6969:6969 fl-admin:dev`
Stop: `docker stop fl-admin`

## Development server

Run `npm run start:dev` for a dev server. Navigate to `http://localhost:6969/`. The app will automatically reload if you change any of the source files.

## Build

Run `npm run build:dev` for dev build, `npm run build:stage` for staging build or `npm run build:prod` for production build.

## Deploy

Run `npm run deploy:dev` for dev deploy, `npm run deploy:stage` for staging deploy or `npm run deploy:prod` for production deploy.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
