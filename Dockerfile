FROM node:8.9.4

RUN mkdir -p /usr/src/app

WORKDIR /usr/src/app

COPY package.json /usr/src/app

RUN npm install

COPY . /usr/src/app

EXPOSE 6969

CMD ["npm", "run", "start:dev"]
