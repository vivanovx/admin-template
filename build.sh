#!/bin/bash

if [ $ENV == 'dev' ]; then
  echo 'Running DEV build..'
  npm run build:dev
elif [ $ENV == 'prod' ]; then
  echo 'Running PROD build..'
  npm run build:prod
elif [ $ENV == 'stage' ]; then
  echo 'Running STAGE build..'
  npm run build:stage
fi
